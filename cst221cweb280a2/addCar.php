<!DOCTYPE html>
<!-- 
Jesse Preiner : CST221
CWEB280 - Shane McDonald && Ernesto Basoalto
generateReport.php

October 23rd 2014
-->
<?php

function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");

// create new form
$addCarForm = new HtmlForm();
$addCarForm->renderStart("carform", "Add a new car");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add car</title>
        <link rel="stylesheet" type="text/css" href="formstyles.css" />
        <script type="text/javascript" src='back.js'></script>
    </head>
    <body>
        <?php
        $dbObject = new DbObject();

        // connection successful
        if ($dbObject) {

            // get all cars
            $carResults = $dbObject->select("*", "cst221_car");

            // overly complex way of capitalizing first letters of names
            $ownerResults = $dbObject->select("ownerID, concat(ucase(left(firstName,1)), "
                    . "lcase(substring(firstName, 2)), ' ', ucase(left(lastName,1)), "
                    . "lcase(substring(lastName, 2)))", "cst221_owner", "", "lastName");

            // create array of owners        
            $owners = DbObject::createArray($ownerResults);

            // helper to fix the boolean issue with numbers
            $permitOptions = Array(
                1 => 'Yes', 0 => 'No'
            );

            // start form
            $addCarForm->renderSelect("ownerid", "Owner", $owners);
            echo "<a href='addOwner.php'>Add new owner</a>";

            // render controls 0--0 used plateNum for primary key, they should all be unique.
            // considered VIN or carID but this should suffice for our purposes.
            $addCarForm->renderTextbox("plateNum", "License plate number (NNNLLL)");
            $addCarForm->renderTextbox("make", "Make");
            $addCarForm->renderTextbox("model", "Model");
            $addCarForm->renderTextbox("year", "Model year");
            $addCarForm->renderSelect("hasPermit", "Has permit?", $permitOptions);
            echo "<div><button id='ownerBack' onclick='goBack(); return false;'>Back</button></div>";
            $addCarForm->renderSubmitEnd("submit", "Submit");

            // if form's been posted, check the input
            if (isset($_POST['submit'])) {

                // if all the fields are filled out
                if (validateCarFields()) {
                    // destroy value in post aray for submit button as to not
                    // try and insert it/
                    unset($_POST['submit']);

                    // insert the car
                    $numRows = $dbObject->insert($_POST, "cst221_car");

                    if ($numRows > 0) {
                        echo "<p>Successfully added car</p>";
                    } else {
                        echo "<p>Nothing Added</p>";
                    }
                } else {
                    echo "<div><p class='error'>Error: Please fill all fields</p></div>";
                }
            }
        } else {
            // death
            die("Can't open");
        }

        /**
         *  Helper method to check if fields are filled in
         * @return bool True if all fields are filled out, otherwise false
         */
        function validateCarFields() {
            return ($_POST['plateNum'] != "" &&
                    $_POST['make'] != "" &&
                    $_POST['model'] != "" &&
                    $_POST['year'] != "" &&
                    $_POST['hasPermit'] != "" &&
                    $_POST['ownerid']);
        }
        ?>

    </body>
</html>