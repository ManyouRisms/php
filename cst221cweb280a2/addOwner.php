<!DOCTYPE html>
<!-- 
Jesse Preiner : CST221
CWEB280 - Shane McDonald && Ernesto Basoalto
addOwner.php
October 23rd 2014

-->
<?php

function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>cst221cweb280a2-addOwner-Oct 22, 2014</title>
        <link rel="stylesheet" type="text/css" href="formstyles.css" />
        <script type="text/javascript" src='back.js'></script>

    </head>
    <body>
        <div>

            <?php
            // create new form
            $addOwnerForm = new HtmlForm();
            
            // controls
            $addOwnerForm->renderStart("ownerform", "New Owner");
            $addOwnerForm->renderTextbox("firstName", "First Name");
            $addOwnerForm->renderTextbox("lastName", "Last Name");
            $addOwnerForm->renderTextbox("phoneNum", "Phone Number");
            $addOwnerForm->renderTextbox("email", "Email Address");
            // back button 
            echo "<div><button id='ownerBack' onclick='goBack(); return false;'>Back</button></div>";
            
            // end form
            $addOwnerForm->renderSubmitEnd("addowner", "Submit");

            if (isset($_POST['addowner'])) {

                // set variables from the post variable
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $phoneNum = $_POST['phoneNum'];
                $email = $_POST['email'];

                $dbObject = new DbObject();


                // connected
                if ($dbObject) {
                    // make sure fields are filled out
                    if (!validateOwnerFields()) {
                        echo "<p class='error'>Please ensure all fields are filled out.</p>";
                    } else {

                        // get the button out of the post array
                        unset($_POST['addowner']);
                        
                        // insert values
                        $numRows = $dbObject->insert($_POST, "cst221_owner");

                        if ($numRows > 0) {
                            echo "<p>Added owner.</p>";
                        }
                        else
                        {
                            echo "<p>Nothing Added</p>";
                        }
                    }
                }
            }
                            
            function validateOwnerFields() {
                return (
                        $_POST['firstName'] != "" &&
                        $_POST['lastName'] != "" &&
                        $_POST['phoneNum'] != "" &&
                        $_POST['email'] != "" );
            }
            ?>

        </div>
        

    </body>
</html>
