<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags

function checkURL($websiteURL)
{
    //$pattern = "#^(http(s)?)://([a-z0-9\.]+\.[a-z]{2,4})(/[\w/-_+%]+)#i";
    $pattern = "#^(http(s)?)://([a-z0-9\.]+\.[a-z]{2,4})(/[\w/-_+%]+)?#i";
    
    
    
    $message = "$websiteURL is not a valid URL <br />";
    
    if (preg_match($pattern, $websiteURL))
    {
        $message = "Yes ( $websiteURL ) is a valid URL.";
    }
    return $message;
    
}

// substitute found patterns in a replacement string
function rearrangeDate($dateString)
{
    // this method will take a string like : "September 11, 2014" and return
    // a string like : "2014-September-11"
    $pattern = "/(\w+) (\d+), (\d{2,4})/i";
    // replacementFormat has placeholders, used to rearrange the sections above
    $replacementFormat = "$3-$1-$2";
    return preg_replace($pattern, $replacementFormat, $dateString);
    
}



?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls6-strings-Sep 11, 2014</title>
    </head>
    <body>
        <h1>lo1-ls6-strings</h1>
        <div>

            <?php
            // display code blocks here. this is where you will echo HTML elements
            // on the web page. try to minimize nesting PHP tags within HTML tags
            echo checkURL("https://mail.google.com/mail/u/0/#inbox");
            
            ?>
            
            <h2>Use Regex to arrange strings</h2>
            <?php 
            echo rearrangeDate("February 09, 2014");
            
            ?>

            <h2>
                String concatenation and substitution in PHP
            </h2>
            <?php 
            $someString = "I'm so pregnant";
            
            // embedded substitution
            //  allows some greatness dealing with open and close quotes for variables
            //ex "<input type='".$type."' id='".$id."' />;
            echo "$someString is substituted using PERL like syntax <br />";
            
            // concatenation using period
            // spread long statements over multiple lines to make nicely formatted
            // php code
            echo $someString . ' is concatenated using the period (.) <br />';
            
            
            // crazy method - HEREDOC
            // historically the most efficient way to define a string
            // nice for keeping html nicely formatted when defining classes and 
            // whatnot later on.
            $someLongString = <<<EOT
                    <p>rgdjh</p>
                    <h1>drgjhdfjgdgfdgdfgd</h1>
                    <div> <h2>bonkers shit dawg its real now g!!!!!! ^(*&^$%$#FGFDfefH\\\\</h2>
                    </div>
EOT;
            echo $someLongString;
            
            ?>
            
            
        </div>
    </body>
</html>
