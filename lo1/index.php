<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-index-Aug 28, 2014</title>
    </head>
    <body>
        <h1>lo1-index</h1>
        <div>  
            <ol>
                <li><a href="ls2-controlstructures.php">1.2 Control Structures</a></li>                
                <li><a href="ls3-htmlforms.php">1.3 Control Structures</a></li>                
                <li><a href="ls4-fileio.php">1.4 File Input/Output</a></li>                
                <li><a href="ls5-arrays.php">1.5 Arrays</a></li>
                <li><a href="ls6-strings.php">1.6 Strings</a></li>
                <li><a href="ls7-functions.php">1.7 Functions</a></li>                
                <li><a href="ls8-email.php">1.8 Email</a></li>                
                
            </ol>

        </div>
    </body>
</html>
