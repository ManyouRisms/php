<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags

//numeric arrays, because the index is a numeric value

//first method for creating array
$pets = array("Buddy", "Murphy", "Chevy", "Java");

//method 2: define a variable with array notation and index
$students[0] = "Paulyle Wallenboechlor";
$students[1] = "Briolin Aprand";
$students[2] = "Dorijessie Prinetsky";

//method 3: for numeric ascention arrays there is no need to specify the index
//  the index is automatically incremented.
$students[] = "Shanesto McBaso";
$students[] = "Alexon Marionstein";

//associative arrays
        
//method 1: using the array function
// key must be a string
$provinces = array("AB"=>"Alberta", "SK"=>"Saskatchewan", "BC"=>"British Columbia"); 

//method  2: using array notation with a string "key" index
$provinces["MB"] = "Manitobah";
$provinces["ON"] = "Ontario";


//2d arrays (chapter 5 section : creating arrays
$customers = array(); //empty array
$customers[] = array("Jason Gilmore", "jason@example.com", "614-999-9999");
$customers[] = array("Jesse James", "jesse@example.com", "818-999-9999");
$customers[] = array("Donald Duck", "donald@example.com", "212-999-9999");




?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls5-arrays-Sep 4, 2014</title>
    </head>
    <body>
        <h1>lo1-ls5-arrays</h1>
        <div>
            <h2>Students</h2>
            

<?php
echo "<p>$students[0]</p>";
echo "<p>$students[1]</p>";
echo "<p>$students[2]</p>";        
?>
        </div>
    
        <!-- Minicise - output students using for loop -->
        <div>
            <h2>
                Students - for loop
            </h2>
<?php 
    for ($index = 0; $index < count($students); $index++) 
{
    echo "<p> {$students[$index]} </p>";
}

?>
            <!-- first method only works in certain php versions-->
            <br/> single output <?= $students[0] ?> 
            <br/> single output <?php echo "$students[0]"; ?>
            
            
            
        </div>
    
        <div>
            <h2>Province names and abbreviations - foreach loop</h2>
            
            
<?php 
    // loop through the associative aray and get the key and value
// stored somewhere in the array using =>
    foreach ($provinces as $abbrev => $province) 
    {
        echo "<p>$province</p>";
        echo "<p>$abbrev</p>";
        
    }
?>
            
            
        </div>
        
        <div>
            <h2>Customers 2d array - with for loop</h2>
            

                <?php 
    foreach ($customers as $customerData) 
    {
        echo "<ul>";
        foreach($customerData as $datum)
        {
            echo "<li>$datum</li>";
        }
        echo "</ul>";
        
    }           
?>
         </div>
        <div>
            <h2>Customers - using vprintf function</h2>
            
<?php 
    foreach ($customers as $customerData) 
    {
        //%s as a placeholder for string data
        vprintf("<ul><li>Name: %s</li><li>Email: %s</li><li>Phone: %s</li></ul>", 
                $customerData);
        // remember $customerData is an array that was nested inside of customers.
        
    }           
?>
  
            
        </div>
        
    </body>
    
</html>
