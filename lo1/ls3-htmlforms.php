<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls3-htmlforms-Sep 2, 2014</title>
        
            <style>
                fieldset {min-width:30em; border: 2px solid blue;}
                legend {margin-left:-.9em ; padding: .5em 1em; border: 2px solid blue;}
                fieldset div{margin: 1em .5em; padding: .25em 0; border-bottom: 1px solid blue;}
                label{width: 10em; display: inline-block} // must indicate inline block
                select,input[type='text']{width: 15em;}
                
            </style>
        
    </head>
    <body>
        <h1>lo1-ls3-htmlforms</h1>
        <div>

<?php
// display code blocks here. this is where you will echo HTML elements
// on the web page. try to minimize nesting PHP tags within HTML tags

// check if form was submitted by checking to see if he submit name has a value.
// normal practice is to check the form
if (isset($_GET['createUser']))
{

    // in this example only we will do some simple checking at the top of the 
    //  page.
    
    $gender = isset($_GET["gender"]) ? $_GET["gender"] : "Not set";
    $terms = isset($_GET["acceptterms"]) ? $_GET["acceptterms"] : "Not set";
    

    // won't use isset for this, because it IS set, but to a  blank string, so
    //  it would be 'true'
    
    
    $firstname = !empty($_GET["firstName"]) ? $_GET["firstName"] : "anonymous";
    $lastname = !empty($_GET["lastName"]) ? $_GET["lastName"] : "anonymous";
    $address = !empty($_GET["address"]) ? $_GET["address"] : "anonymous";
    
    
        
    echo "<h2>Success, form submitted.</h2>";
    echo "<br/>Your first name is $firstname";
    echo "<br/>Your last name is $lastname";
    echo "<br/>Your gender is $gender";
    echo "<br/>Your address is $address";
    echo "<br/>Your province is $_GET[province]";
    echo "<br/>Did you agree to the terms: $terms";
    
}
else 
{
?>
            
            <!-- id is for client, name is for server -->
            <!-- for this example only we use get uinstead of post -->
            
            <form id="frmUser" name="frmUser" action="ls3-htmlforms.php" method="GET" >
                <fieldset>
                    <!-- acts as a title -->
                    <legend>User info</legend>
                    <div>
                        <!-- the 'for' attribute makes you able to click
                         the label and it will highlight the box-->
                        <label for="txtFirstName" >First Name</label>
                        <input type="text" name="firstName" id="txtFirstName" 
                               maxlength="40" />                      
                    </div>
                    <div>
                        <!-- the 'for' attribute makes you able to click
                         the label and it will highlight the box-->
                        <label for="txtLastName" >Last Name</label>
                        <input type="text" name="lastName" id="txtLastName" 
                               maxlength="40" />                      
                    </div>

                    <div>
                        <!-- the 'for' attribute makes you able to click
                         the label and it will highlight the box-->
                        <label>Gender</label>
                        <input type="radio" id="rdoGenderMale" for="rdoGenderMale" name="gender" value="male"><label>Male</label>
                        <input type="radio" id="rdoGenderFemale" for="rdoGenderFemale" name="gender" value="female"><label>Female</label>                      
                    </div>
                    
                    <div>
                        <!-- the 'for' attribute makes you able to click
                         the label and it will highlight the box-->
                        <label for="txtAddress" >Address</label>
                        <input type="text" name="address" id="txtAddress" 
                               maxlength="40" />                      
                    </div>
                    
                    <!-- Minicise - add a select box for the western provinces 
                     Option text should be full province name
                     Option value should be 2 letter Abbreviation
                    
                    -->
                    <div>
                        <label>Province</label>
                        <select name="province" id="selProvince">                            
                            <option value="ab">Alberta</option>
                            <option value="bc">British Columbia</option>
                            <option value="mb">Manitoba</option>
                            <option value="sk">Saskatchewan</option>
                   
                        </select>
                        <div>
                            <label>Accept Terms</label>
                            <input type="checkbox" value="yes" name="acceptterms" id="chkAcceptTerms" ><label for="chkAcceptTerms">Yes, I accept</label>
                            
                        </div>
                        
                    </div>
                    
                    
                    <div>
                        <input type="submit" id="btnSubmit" name="createUser" 
                               value="Create User" />
                        <input type="reset" id="btnReset" value="Clear" />
                        <!-- reset button does not submit to server -->
                        
                    </div>
                    

                    
                    
                </fieldset>
            </form>
<?php } ?>            

        </div>
    </body>
</html>
