<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags

function outputHello()
{
    echo "<p>Hello, functions</p>";
}

//defining parameters in PHP functions
function getUserMotto($name, $age, $motto)
{
    return "<p>Hello, my name is $name. I am $age years old. I live by
        the motto: $motto</p>";
}

//defining default parameters in PHP
function outputUserMotto($name, $age=22, $motto="What rhymes with hug me")
{
    echo getUserMotto($name, $age, $motto);
    
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls7-functions-Sep 4, 2014</title>
    </head>
    <body>
        <h1>lo1-ls7-functions</h1>
        <div>

            <?php
            // display code blocks here. this is where you will echo HTML elements
            // on the web page. try to minimize nesting PHP tags within HTML tags
            
            outputHello();
            echo getUserMotto("jesse", 28, "You only YOLO once");
            outputUserMotto("Paulyle");
            ?>

        </div>
    </body>
</html>
