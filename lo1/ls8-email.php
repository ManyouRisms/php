<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags

            function sendMail($to, 
                    $subject="Email from PHP", 
                    
                    $message="Message from PHP!!", 
                    $senderName="Jesse Preiner", 
                    $senderEmail="cst221@cst.siast.sk.ca")
            {
                // the header needs to be modified to include the reply-to and 
                // format the email as html - look it up on php.ca
                
                $header = "From: $senderName<$senderEmail>";
                return mail($to, $subject, $message, $header);
                
            }
            
            //php uses the mail function to send emails using the specified email
            // server in the config.ini file for PHP.
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls8-email-Sep 4, 2014</title>
    </head>
    <body>
        <h1>lo1-ls8-email</h1>
        <div>

            <?php
            // display code blocks here. this is where you will echo HTML elements
            // on the web page. try to minimize nesting PHP tags within HTML tags
            if (sendMail("cst221@cst.siast.sk.ca"))
            {
                echo "Done deal. Email sent";
            }
            else
            {
                echo "Server didn't respond. Try again later";
            }

            
            
            
            ?>

        </div>
    </body>
</html>
