<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls2-controlstructures-Aug 28, 2014</title>
    </head>
    <body>
        <h1>lo1-ls2-controlstructures</h1>
        <h2>Conditional Statements</h2>
    
        <div>
        <?php 
        //for this example we will use a random number to base our conditional 
        //statements on
        
        $rnd = rand(1, 10);
        if ($rnd <= 5 )
        {
            echo "The random number is $rnd and is less than or equal to 5 ";
            
        }
        elseif ($rnd <= 7)
        {
            echo "The random number is $rnd and is less than or equal to 7";
        }
        else
        {
            echo "The random number is $rnd and is lses than or equal to 10 ";
        }
        ?>
        </div>
        
        <h2>Minicise</h2>
        <div>
            Use conditional statements to display whether the random number is 
            odd or even. If odd, make the font color green, otherwise color is
            red.
            
            <?php 
            if ($rnd % 2 == 0)
            {
                echo "<p style='color: green'>The random number is even</p>";
            }
            else
            {
                echo "<p style='color: red'>The random number is odd</p>";
            }
            
            ?>
        </div>
        
        <h2>Switch Statements</h2>
        <div>
            <?php 
            // in this example we will first set a variable with a category name
            // then we will use a URL parameter as the key for the switch statement
            
            //$category = "weather";
            
            $category = isset($_GET["cat"]) ? $_GET["cat"] : "";
            
            switch ($category)
            {
                case "news": 
                    echo "What's happening around the world.." ;
                    break;
                case "sports":
                    echo "Check out the latest sports highlights";
                    break;
                case "weather":
                    echo "Obviously cold and shitty";
                    break;
                default:
                    echo "No match found";
                    break;
            }
            
            ?>
        </div>
        
        <h2>
            Minicise
        </h2>
        <div>
        Use a switch to display whether the random number is :
            less than or equal to 3.
            greater than 3 but less than or equal to 6.
            greater than 6 but less than or equal to 10.
            
            <?php 
            
            
                if ($rnd <= 3)
                {
                    $answer = "first";
                }
                elseif ($rnd > 3 && $rnd <= 6)
                {
                    $answer = "second";
                }
                else
                {
                    $answer = "third";
                }
            switch ($answer)
            {
                case "first":
                    echo "less than or equal to 3.";
                    break;
                case "second":
                    echo "greater than 3 but less than or equal to 6.";
                    break;
                case "third":
                    echo "greater than 6 but less than or equal to 10.";
                    break;
                default:
                    echo "i donno then";
                    break;
                                      
            }
            ?>

        </div>
        
        <h2>While loops</h2>
        <ol>
        <?php 
        $count = 1; //count variable to track the loop
        $sum = 0; // sum of all numbers in the loop
//        
//        //loop while counter is less than random number
//        while ($count < $rnd)
//        {
//            $sum += $count;
//            $count++;
//        }
        //echo "$sum";
        
        ?>
        </ol>
        <h2>Minicise - For loops</h2>
        <p>Do the same as above using a for loop</p>
        
        <?php 
 //       for ($count = 1; $count <= $rnd; $count++)
   //     {
   //         $sum += $count;
   //    }
   //     echo "$sum";
   //     ?>
        <h2>Minicise - Do while loop</h2>
        <?php 
        do 
        {
            $sum += $count;
             $count++;
            
        }while ( $count <= $rnd);
         
        echo "$sum";
        ?>
        
        
    </body>
</html>
