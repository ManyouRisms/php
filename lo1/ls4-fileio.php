<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags


//define global variables
$path = "../wide_open/";
$file = null; // indicator variable showing that the user uploaded a file or not
                // if a file is uploaded we will set it to the corresponding
                // value (in this example: the filename we save into the wide_open 
                // folder.)


//function to handle upload. responsibble for checks, copying file over to correct
// location.
function handleUpload()
{
    global $path, $file; // this tells the function to use pre-defined global 
                            // variables
    $returnMsg = "No file uploaded or even submitted";
    // we need to perform some checks. first: make sure submitted, second: make
    // sure the file is uploaded (actually on the server)
    
    if(isset($_POST["processfile"]) && is_uploaded_file($_FILES["upload"]["tmp_name"])) //"upload" is the key from our name value in the form
                                                                                                // "tmp_name" is the guid name of the file as it                                                                                             // exists in tmp
    {    
    //minicise : ensure the file is plain text file - hint : look up mime for plain text     
    //minicise - check the filesize to ensure it is not greater than 1024 bytes.    
        $fileSize = $_FILES["upload"]["size"];
        $fileType = $_FILES["upload"]["type"];
        
        if ( $fileSize <= 1024)
        {
            //danger of having the same name
           $file=$path.$_FILES["upload"]["name"]; // gets the friendly name of the file                    
           // also the location where we are moving the file

           //only moves uploaded file if it is a valid uploaded file. (is_upload_file wasn't really overly necessary)
           // also it returns true or false, so we can make decisions based on its succerss

           if (move_uploaded_file($_FILES["upload"]["tmp_name"] , $file))

           // for the second dimension in the $_files super global the following are allowed:
           // tmp_name - the path and file name to the server temp location for the uplaoded file
           // name - the name displayed to the user when they selected the file fo rupload
           // size - the size of the file in bytes
           // type - the MIME type of the file - a string representation (ex text/html")
           {
               if ( $fileType !== "text/plain")
               {
                   $returnMsg = "Sorry, only text files allowed";
               }
           }
               $returnMsg = "File was uploaded and moved to target folder";
           }
        else
        {
            $returnMsg = "File was submitted and uploaded but failed to move";
        }
   
}
else
{
    $returnMsg = "The file was larger than 1KB";
}

    return $returnMsg;
}

// method 1: oldschool open file read line by line and close the file
function readFileByLine()
{
    global $file;
    if ($file) // remember was initialized to null, if it was moved properly,
                // it shouldn't evaluate to false, otherwise it will.
    {
        $fileHandler = fopen($file, "r"); // mode 'r' means readonly starting at the SOF;
        
        while(!feof($fileHandler)) // while we are not at the end of the file
        {
            echo fgets($fileHandler, 1024); // performance tuning, reads a maximum
                                            // of 1KB of a line in case it is a huge mother.
                                            // or until it reaches "\n"
        }
        // close the file when you are done reading
        fclose($fileHandler); 
    }
}

// method 2: useful for reading flat data files.
function readFileByArray()
{
    global $file;
    if ($file)
    {
        $lines = file($file); // the file function reads the file and returns an 
                              // array of lines.
                              foreach ($lines as $line) {
                              echo "$line";
                                  
                              }
    }
}

// method 3: read file all at once
function readFileAll()
{
    global $file;
    if ($file)
    {
        echo file_get_contents($file);
    }
}

// we are going to use fopen to append and to overwrite an uploaded file.
// illustrate a method that can append text to an existing file
function appendToFile($newLine)
{
    global $file;
    if ($file)
    {
        $fileHandler = fopen($file, "a"); // "a" means open the file in append mode
        // append mode and the pointer starts at the EOF
        fwrite($fileHandler, $newLine); // append parameter passed in to the file
        fclose($fileHandler); // close the file
    }
    
}

function overwriteFile($newLine)
{
    global $file;
    if ($file)
    {
        $fileHandler = fopen($file, "o"); // "o" means open the file in overwrite mode
                                            // puts pointer to start of file
        fwrite($file, $newLine); // 
        fclose($fileHandler); // close the file
    }
   
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo1-ls4-fileio-Sep 8, 2014</title>
    </head>
    <body>
        <h1>lo1-ls4-fileio</h1>
        
        
        <!--  -->
        <!-- need enctype when we are dealing with binary -->
        <form method="POST" enctype="multipart/form-data" action="ls4-fileio.php" >
            <legend>Upload File</legend>
            <fieldset>
                <div>
                    <label for="filUpload" >Select File</label> <!-- REMEMBER THIS NESTING FORMAT FOR ASSIGNMENT -->
                    <input name="upload" id="filUpload" type="file" /> <!--this name will be the index for superglobals -->
                </div>
                <div>
                    <label for="txtAdd">Add Text </label>
                    <!--  multi line text box -->
                    <textarea name="addtext" id="txtAdd"> 
                        
                    </textarea>
                        
                </div>
                
                <div>
                    <!-- again in this example,we will check to see if the form was submitted
                        by checking if the submit button is defined (quick way)-->
                    <input type="submit" value="Upload and process file" name="processfile" />
                    
                </div>
            </fieldset>
            
        </form>
        
            <?php
                echo handleUpload();
                if (!empty($_POST["addtext"]))
                {
                echo "<hr />";
                echo "You submitted the following text: <br /> $_POST[addtext]";
                
                appendToFile($_POST["addtext"]); // broken?
                }
            ?>
        
        <h2>Read the file line by line and output to the page</h2>

        <pre><?php 
readFileByLine();
        ?>
        </pre>
        <h2>Read file to array and print line by line from array</h2>
        <pre><?php 
        readFileByArray();
        
        ?>

        </pre>
        <h2>Read file all at once</h2>
        <pre><?php 
                    readFileAll();
            ?>
        </pre>
        

        

    </body>
</html>
