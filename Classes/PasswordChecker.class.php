<?php
/**
 * The PasswordChecker allows us to check if an entered username and
 * password is valid.
 * @author ins214
 */
class PasswordChecker
{
    private $password;
    const PRIVILEGE_DEFAULT = 1;

    /**
     * Purpose: Create a password checker object.
     */
    public function __construct()
    {
    }
    
    /**
     * Purpose: Determine whether the supplied username and password
     *   combination is valid
     * @param string $username The username to check
     * @param string $password The password to check
     * @return boolean TRUE if the username / password combination is valid,
     *   and FALSE otherwise
     */
    public function isValid( $username, $password )
    {
        // By default, the combination is not valid
        $valid = FALSE;
        
        // Open a database connection
        //$db = new DbObject();
        $db = new DbObject("localhost", "root", "X41Ak7123", "assignment3");
                
        // Query for the password for the specified username
        $qryResult = $db->select( "password", "Member",
                "username='" . $db->escape( $username ) . "'" );
        
        // If there was one row returned, check the password against
        // the supplied password
        if ( $qryResult->num_rows == 1 )
        {
            $passwordRow = $qryResult->fetch_row();
            if ( $passwordRow[0] == password_verify( $password,
                                                     $passwordRow[0] ) )
            {
                $valid = true;
            }
        }
        
        // Return whether the username and password combination is valid
        return $valid;
    }

    /**
     * Purpose: Add a user into the password list
     * @param string $username The username to add
     * @param string $password The password associated with the username
     * @return boolean TRUE if the user was successfully added,
     *   FALSE otherwise
     */
//    public function addUser( $username, $password, $email="" )
//    {
//        $db = new DbObject("localhost", "root", "X41Ak7123", "assignment3");
//        $encoded= password_hash( $password, PASSWORD_DEFAULT );
//        
//        //build array
//        $record["username"] = $db->escape( $username );
//        $record["password"] = $encoded;
//        $record["email"] = $email;
//        $record["privilege"] = PasswordChecker::PRIVILEGE_DEFAULT;
//                
//        $numRows = $db->insert( $record, "Member" );
//
//        // false if nothing happened
//        return ( $numRows == 1 );
//    }
    
    public function addUser( $username, $password, $email)
    {
        // Open a database connection
        $db = new DbObject("localhost", "root", "X41Ak7123", "assignment3");

        // Create the array to use with the insert method
        $record["username"] = $db->escape( $username );
        $record["password"] = password_hash( $password, PASSWORD_DEFAULT );
        $record["email"] = $email;
        $record["privilege"] = PasswordChecker::PRIVILEGE_DEFAULT;        
        // Insert the user into the Password database
        $numRows = $db->insert( $record, "Member" );

        return ( $numRows == 1 );
    }    

    /**
     * Helper to see if an account exists already.
     * @param string $username To check the db if it exists
     * @return boolean False if username exists, true otherwise.
     */
    public function validUser($username)
    {
        $exists = true;
        $db = new DbObject("localhost", "root", "X41Ak7123", "assignment3");
        $selectUser= "select username from Member where username='"
                . $username . "'";
        
        $userCheckResult = $db->runQuery($selectUser);
        if ($userCheckResult->num_rows > 0)
        {
            $exists=false;
        }
        return $exists;
    }
    
        
    public function displayPrivilegeLevel($username)
    {
        $db = new DbObject("localhost", "root", "X41Ak7123", "assignment3");
        //$sqlUsername = $db->escape_string($username);
        $stmt = "select privilege from member where username='$username'";
        $result = $db->runQuery($stmt);
        $row = $result->fetch_row();
        $level = $row[0];
        return $level;
    }
}
