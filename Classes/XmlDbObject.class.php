<?php
/**
 * Description of XmlDbObject
 * XML db object extends the db object to query
 * the db and return xml
 * @author cst221
 */

require_once 'DbObject.class.php';
class XmlDbObject extends DbObject 
{
    /**
     * Purpose: To select data from the db and return as XML string
     * @param type $rootName - Name of the root node in the xml file
     * @param string $columnList list of columns to be selected
     * @param string $tableList list of tables to select from
     * @param string $condition optional SQL condition to select with
     * @param string $sort optional SQL sort statement to apply
     * @param string $other optional any other SQL clauses to apply
     * @param type $childName - optional, the name of the node
     *  one level below the root node
     * @param type $xsltPath
     * @return string The actual xml text
     */
    public function selectToXml($rootName, $columnList, $tableList,
            $condition="", $sort="", $other="", $childName="row", $xsltPath="")
    {
        // get data from specified tables (calling parent's select method)
        $queryResult = $this->select($columnList, $tableList, $condition, $sort, $other);
        
        // need starting every xml file
        $xmlDeclare = '<?xml version="1.0" encoding="UTF-8"?>';
        
        // use xsl if we have it
        if  (!empty($xsltPath))
        {
            $xmlDeclare .= "<?xml-stylesheet type='text/xsl' href='$xsltPath' ?>";
            
        }
        
        // make the start of the xml file
        $rootNode = new SimpleXMLElement($xmlDeclare . "<$rootName />");
        
        // loop through the queer result and set the xml elements and attributes
        while ($rowData = $queryResult->fetch_assoc())
        {   
            // create a new child node to store the row's data
            $childNode= $rootNode->addChild($childName);
            
            // foreach item in the row data assoc array
            // works out to be for each column in the select, we create a new
            // child node
            foreach ($rowData as $nodeName=>$nodeValue)
            {
                // need to escape the xml special chars using htmlentities
                
                $childNode->addChild($nodeName, htmlentities($nodeValue));
            }
            
        }
        // free resources
        $queryResult->free();
        
        return $rootNode->asXML();
        
        
        
        return "";

    }
}
