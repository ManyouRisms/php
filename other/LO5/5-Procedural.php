<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LO5: Retrieving data from a database procedurally</title>
    </head>
    <body>
        <h1>LO5: Retrieving data from a database procedurally</h1>
        <?php

$db = mysqli_connect( "10.20.38.43", "shane", "hulk", "shane" );

if ( mysqli_connect_errno() ) {
    echo "<p>Failed to connect to database: " . mysqli_connect_error() .
            "</p>\n";
    exit();
} else {
    echo "<p>Successfully connected to the database!</p>\n";
}

// Query the database
$qryResults = mysqli_query( $db,
        "SELECT firstName, lastName FROM Instructor" );

// IF the query succeeded
if ( $qryResults ) {
    // Determine the number of rows returned and display it
    $numRows = mysqli_num_rows( $qryResults );
    printf( "<p>Fetched %d rows from the Instructor table</p>\n", $numRows );
    
    // We're done with the results -- free them
    mysqli_free_result( $qryResults );
} else {
    // Indicate that we failed to retrieve the results
    echo "<p><blink>Failed to fetch from the Instructor table</blink></p>\n";
}

mysqli_close( $db );

        ?>
    </body>
</html>
