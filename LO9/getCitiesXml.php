<?php

require_once '../classes/XmlDbObject.class.php';

// tell the browser to expect xml content (expects html by default)
header('Content-type: text/xml');

// get xml content from the db server sing the xmldbobject

$xmlDb = new XmlDbObject();

// add code to get the cities for the specified provid

$provid = 0;
if (isset($_GET['provid']))
{
    $provid = $xmlDb->escape($_GET['provid']);
}

$xmlString = $xmlDb->selectToXml("cities", "cityid as id, city as name",
        "Cities", "provinceid=$provid", "City ASC", "", "city");

echo $xmlString;

