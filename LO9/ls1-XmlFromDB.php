<?php
function my_autoloader($className) {
    require_once( "../classes/$className.class.php" );
}

spl_autoload_register("my_autoloader");

// db object
$db = new DbObject();

// query the provines table to get the data we need
$queryResult = $db->select("provinceID, province as name, code, population, density", "Provinces",
        "", "province" );

// province was stored in attribute called name in xml, so we alias it to help
// keep it straight

//create simplexml element
$rootNode = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?>'
        . "<provinces />");

// loop through the queer result and set the xml elements and attributes
while ($rowData = $queryResult->fetch_assoc())
{
    $provNode = $rootNode->addChild("province");
    // set the elements and attributes from the rowData array
    
    $provNode->addAttribute("name", $rowData["name"]);
    $provNode->addChild("code", $rowData["code"]);
    $provNode->addChild("population", $rowData["population"])->
               addAttribute("density", $rowData["density"]);
    
}

// free up query resources
$queryResult->free();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO9-ls1-XmlFromDB-Nov 24, 2014</title>
    </head>
    <body>
        <h1>LO9-ls1-XmlFromDB</h1>
        <div>
            <h2>following xml from simplexml</h2>
            <code><pre>

            <?php
                echo htmlspecialchars($rootNode->asXML());
            ?>
            </pre></code>
        </div>
        
                <div>
                    <h2> following xml from xmldbobject</h2>
            <code><pre>

            <?php
                $xmlDb = new XmlDbObject();
                $xmlString = $xmlDb->selectToXml("provinces", "provinceID as id, "
                        . "province as name, code, population, density", 
                        "Provinces",
                        "", "province", "", "province");
                echo htmlspecialchars($xmlString);
            ?>
            </pre></code>
        </div>
    </body>
</html>
