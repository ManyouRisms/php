<?php
require_once '../classes/XmlDbObject.class.php';

// tell the browser to expect xml content (expects html by default)
header('Content-type: text/xml');

// get xml content from the db server sing the xmldbobject

$xmlDb = new XmlDbObject();

$xmlString = $xmlDb->selectToXml("provinces", "provinceid as id, province as name",
        "Provinces", "", "Province ASC", "", "province", "./provIDList.xsl");

echo $xmlString;

