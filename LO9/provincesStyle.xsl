<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : provincesStyle.xsl
    Created on : November 25, 2014, 9:26 AM
    Author     : cst221
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Provinces of Canada</title>
                <link rel="stylesheet" type="text/css" href="form-table.css"/> 
            </head>
            
            <body>
                <h2>Provinces of Canada</h2>
                <table>
                    <tr>
                        <th>Province</th>
                        <th>Code</th>
                        <th>Cities</th>
                        <th>Flag</th>
                    </tr>
                    <!--Loop through province nodes -->
                    <xsl:for-each select="provinces/province">
                        <!--Add row for each province we have -->
                        <tr>
                            <!-- @ because attribute-->
                            <td><xsl:value-of select="@name" /></td>
                            <td><xsl:value-of select="code" /></td>
                            <td>
                                <ul>
                                <xsl:for-each select="cities/city">
                                    <li><xsl:value-of select="@name" /> -- </li>
                                    <xsl:value-of select="@description" />
                                </xsl:for-each>
                                </ul>
                            </td>
                            <td><xsl:if test="string-length(flag) !=0">
                                <img alt="flag of {@name}" src="data:{flag/@mime}
                                    ;base64,{flag}" />    
                            </xsl:if>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
