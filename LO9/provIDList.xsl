<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : provIDList.xsl
    Created on : November 26, 2014, 11:21 AM
    Author     : cst221
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>provIDList.xsl</title>
                <link rel="stylesheet" type="text/css" href="form-table.css"/> 
            </head>
            <body>
                <h2>Province List</h2>
                <ul>
                    <xsl:for-each select="provinces/province">
                        <li>
                            <xsl:value-of select="id" />
                                <a href='getCitiesXml.php?provid={id}'>
                                    <xsl:value-of select='name' />
                                </a>
                                <ul>
                                    <xsl:for-each select="cities/city">
                                        <ul><xsl:value-of select='name' /> </ul>
                                    </xsl:for-each>
                                </ul>
                                
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
