/***
 * 
 * Jesse Preiner
 * CST221
 * CWEB280
 * Shane McDonald && Ernesto Basoalto
 */
$(function() {
    /**
     * Method to create and return the select box populated
     * with privilege levels.
     * @returns {String} The select box for output.
     */
    var createSelectBox = function () {
        var selBox =
            "<select class='selBox'>" +
            "<option value='1'>Pending</option>" +
            "<option value='2'>Approved</option>" +
            "<option value='3'>Admin</option>" +
        "</select> ";
        return selBox;
    };    
    
    
    var discoverText = function(currentText) {
        return (currentText === 1) ?
                'Pending' : (currentText === 2) ? 'Approved' : 'Admin';
    };

    // select the cells containing privs
    $privCells = $('#results tr').find('td:nth-child(3)');

    // change text based on current text
    $.each($privCells, function() {
        $currText = parseInt($(this).text());
        $(this).html("<div class='permaHidden'> "+ $currText + "</div>" + discoverText($currText) );
    });

    // create the change header
    $("#results tr:first").append("<th>Change</th>");
    
    // append the buttons
    $("#results tr:gt(0)").append(
        "<div class='change'><td><a class='btnUpdate '>Update </a>" +
            "<a class='btnDelete '>Delete</a>\n\</div>\n\
         <div class='change invisible'><a class='btnModify'>Modify </a><a class='btnCancel'>Cancel</a><div>\n\
            </div></td>");
    
    $('.btnUpdate').click(function() {
        toggle($(this).parent());
        
        // get the privilage cell for the current row of the table
        var currRow = $(this).parent().parent().index() + 1;
        var $row = $('#results tbody tr:nth-child(' + currRow + ')');
        var privCell = $row.children('td').eq(2); 
        var privNumber = $(privCell).children().text().toString();
        $(privCell).html(
                createSelectBox(privNumber)
        );
    });

    $('.btnCancel').click(function() {
        cancel($(this));
    });
    
        
    /**
     * 
     * Function which appends the get variables necessary
     * for modification of privilege
     * 
     */
    var cancel = function(jqObject) {
        toggle(jqObject.parent());
        var currRow = jqObject.parent().parent().index() + 1;
        var $row = $('#results tbody tr:nth-child(' + currRow + ')');        
        var privCell = $row.children('td').eq(2); 
        privCell.text(discoverText());
    };
    
    //click handler for modify buttons
    $('.btnModify').click(function() {
        modify( this);
    });
    
    /**
     * 
     * Function which appends the get variables necessary
     * for modification of privilege
     * 
     */
    var modify = function(object) {
        var currRow = $(object).parent().parent().index() + 1;
        var $row = $('#results tbody tr:nth-child(' + currRow + ')');        
        var email = $row.children('td').eq(1).text();
        var newValue = $('select').val();
        var getModify = "index.php?email=" + email + "&newValue=" + newValue;
        location.replace(getModify);
    };
    
    /**
     * Toggles visibility for butons
     * 
     */
    var toggle = function() {
        $('.change').toggleClass("invisible");
    };

    /**
     * onclick for the delete button, creates confirmation and calls
     * delete function
     * 
     */
    $('.btnDelete').click(function() {
        var currRow = $(this).parent().parent().index() + 1;
        var $row = $('#results tbody tr:nth-child(' + currRow + ')');  
        var name = $row.children('td').eq(0).text();                 
        var email = $row.children('td').eq(1).text();        
        var confirmDelete = window.confirm(
                "You are about to delete the user " + name + 
                ", are you sure?"
                );
        
        if (confirmDelete) {
            // delete him/her. 
            deleteUser(email);
        }
    });

    /**
     * appends the delete information necessary to the url and redirects
     */
    var deleteUser = function(email) {       
        location.replace("index.php?deleteUser=" + email );
    };
});