<?php
session_start();
require 'Common.php';

function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");

/**
 * helper function to destroy session and unset get logout
 */
function logout() {
    unset($_SESSION["loggedIn"]);
    $_GET["logout"] = false;
    unset($_GET["logout"]);
    session_destroy();
}
?>

<!DOCTYPE html>
<!--
Jesse Preiner
CST221
CWEB280Assignment3
Shane McDonald && Ernesto Basoalto
November 13 2014
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <link href="style.css" rel="stylesheet" type="text/css"/>   
        <link href="form-table.css" rel="stylesheet" type="text/css"/>        
        <script src="js/behavior.js" type="text/javascript"></script>


    </head>
    <body>
        <div id="pageWrap">
            <?php
            // db for displaying table
            $db = new DbObject();


            // delete user if required
            if (!empty($_GET["deleteUser"])) {
                $email = $_GET["deleteUser"];
                $condition = "email='$email'";
                $db->delete("Member", $condition);
                unset($_GET["deleteUser"]);
            }

            /**
             * if user is set, we want to do an update, find key and update
             *
             */
            if (isset($_GET["email"])) {
                $email = $_GET["email"];
                $emailCondition = " email = '" . filter_var(strip_tags($email),FILTER_SANITIZE_EMAIL ) . "'";
                $privilege = intval($_GET["newValue"]);
                $string = "update Member set privilege='" . $privilege . "' "
                        . " where $emailCondition";
                
                $db->runQuery($string);                
                
                if (!headers_sent()) {
                    header("Location: index.php");
                    die();
                } else {
                    echo "<script> location.replace('index.php'); </script>";
                }
            }

            // if logout is set, clear the url and logout
            if (isset($_GET["logout"]) && $_GET["logout"]) {

                if (!headers_sent()) {

                    header("Location: index.php");
                    die();
                } else {
                    echo "<script> location.replace('index.php'); </script>";
                }

                logout();
            }

            // if a username is set get a reference
            if (isset($_SESSION["userName"])) {
                $pntrUser = $_SESSION["userName"];

                // if we're also logged in,
                if (isset($_SESSION["loggedIn"])) {
                    if ($_SESSION["loggedIn"]) {

                        // get a reference to the users privilege level
                        $privLevel = $_SESSION["privLevel"];
                        switch ($privLevel) {

                            // for pending users
                            case 1: {
                                    echo displaySessionInfo($pntrUser);
                                    echo "<h1 class='welcome'>Welcome, $pntrUser</h1>";
                                    echo "<div class='accountInfo'><h2>Account Pending</h2>";
                                    echo "<p>Your account has not yet been approved."
                                    . "</p></div>";
                                    break;
                                }

                            // for users
                            case 2: {

                                    echo displaySessionInfo($pntrUser);
                                    echo "<h1 class='welcome'>Welcome, $pntrUser</h1>";
                                    $result = $db->runQuery("select username as 'Name', "
                                            . "email as 'Email' from Member order by "
                                            . "username, email;");
                                    echo "<div id='tableContainer'>";
                                    $db->displayRecords($result);
                                    echo "</div>";
                                    break;
                                }

                            // for admins
                            case 3:
                                echo displaySessionInfo($pntrUser);
                                echo "<h1 class='welcome' id='banner'>Welcome, $pntrUser</h1>";
                                $result = $db->runQuery(
                                        "select username as 'Name', "
                                        . "email as 'Email', privilege as 'Privilege'"
                                        . " from Member order by "
                                        . "privilege, username, email;");
                                echo "<div id='tableContainer'>";
                                $db->displayRecords($result);
                                echo "</div>";
                                break;
                        }
                    }
                }
            } else {
                echo "<div id='headerContainer'><div id='unfriendly'><p><a href='register.php'>Register</a></p>";
                echo "<p><a href='login.php'>Login</a></p></div>";
                echo "<div id='memSys'><h1>CST Membership System</h1>"
                . "<h2>...Where dreams go to die</h2></div></div></div>";
            }
            ?>
    </body>
</html>
