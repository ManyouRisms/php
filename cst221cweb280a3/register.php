<?php
session_start(); // start session
//
// load support classes

if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"]) {


    header("HTTP/1.1 301 Moved Permanently");
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}
if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"]) {
    header("Location: index.php");
}

require('Common.php');

function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");
?>

<!DOCTYPE html>
<!--
Jesse Preiner
CST221
CWEB280Assignment3
Shane McDonald && Ernesto Basoalto
November 13 2014
-->
<html>
    <head>
        <link href="form-table.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>

        <meta charset="UTF-8">
        <title>Register now!</title>

    </head>
    <body>
        <?php
        // if the form hasn't yet been submitted
        if (!isset($_POST['registerSubmit'])) {
            // initialize form
            $registerForm = new HtmlForm();
            $registerForm->renderStart("registerForm", "Register for the CST Membership System");

            // render controls
            $registerForm->renderTextbox("username", "Username", true);
            $registerForm->renderTextbox("email", "Email address", true);
            $registerForm->renderPassword("password", "Password", true);
            $registerForm->renderPassword("retype", "Re-type", true);
            $registerForm->renderSubmitEnd("registerSubmit", "Register", "Clear");
            $registerForm->renderEnd();
        }
        // has been submitted, lets check it out
        else {

            // if all fields are filled out 
            if (isset($_POST["password"]) 
                    && isset($_POST["retype"]) 
                    && isset($_POST["username"]) 
                    && isset($_POST["email"]) 
                    && strlen($_POST["username"]) > 2  // username long enough
                    && strlen($_POST["password"]) > 6) { // 6 character password

                // validation for email
                $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?'
                        . '[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x'
                        . '00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}'
                        . '@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39'
                        . '\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B'
                        . '\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:'
                        . '\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-'
                        . '\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]'
                        . '+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21'
                        . '\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22'
                        . ')))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:'
                        . '-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|('
                        . '?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:'
                        . 'IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!'
                        . '(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]'
                        . '{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5}'
                        . ')?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4})'
                        . '{5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::'
                        . '[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{'
                        . '1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:'
                        . '1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2'
                        . '[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))'
                        . '$/iD';
               
                // if a valid email address
                if (preg_match($pattern, $_POST["email"])) {
                    // if passwords match
                    if ($_POST["password"] == $_POST["retype"]) {
                        $passwordChecker = new PasswordChecker();


                        // get user information
                        $user = &$_POST["username"];
                        $pwd = &$_POST["password"];
                        $email = &$_POST["email"];

                        // if the username is unique
                        if ($passwordChecker->validUser(strip_tags($user))) {

                            // username will already be stripped of tags
                            $success = $passwordChecker->addUser($user, $pwd, $email);

                            // user successfully added, redirect.
                            if ($success) {
                                $_SESSION["loggedIn"] = true;
                                $_SESSION["userName"] = $user;
                                $_SESSION["privLevel"] = $passwordChecker->displayPrivilegeLevel($user);
                                
                                // redirect
                                if (!headers_sent()) {

                                    header("Location: index.php");
                                    die();
                                } else {
                                    echo "<script> location.replace('index.php');"
                                    . " </script>";
                                }

                                // wasn't added
                            } else {
                                displayError("<p>Error adding $user as a member</p>");
                            }
                        }

                        // not unique
                        else {
                            displayError("<div class='error'><p>Error, "
                                    . "user $user already exists!</p>");
                        }

                        // passwords don't match
                    } else {

                        displayError("<div class='error'><p>Oops! The verify re-type"
                                . " password field didn't match your"
                                . " password. </p>");
                    }
                } else {
                    displayError("<div class='error'>Invalid email address.</div>");
                }
            } else {
                displayError("<div class='error'>Oops. Some of the fields "
                        . "weren't filled out, or were incomplete.");
            }
        }
        ?>

        <div>    
            <p class='extraInfo'>
                Already have an account? Click <a href='login.php'>here</a> to
                log in.
            </p>
        </div>  

    </body>
</html>
