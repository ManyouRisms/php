<!--
Jesse Preiner
CST221
CWEB280Assignment3
Shane McDonald && Ernesto Basoalto
November 13 2014
-->
<?php
/**
 * The PasswordChecker allows us to check if an entered username and
 * password is valid.
 * @author cst221
 */
class PasswordChecker
{
    private $password;
    private $db;
    const PRIVILEGE_DEFAULT = 1;

    /**
     * Purpose: Create a password checker object.
     */
    public function __construct()
    {
        $this->db = new DbObject();
    }
    
    /**
     * Purpose: Determine whether the supplied username and password
     *   combination is valid
     * @param string $username The username to check
     * @param string $password The password to check
     * @return boolean TRUE if the username / password combination is valid,
     *   and FALSE otherwise
     */
    public function isValid( $username, $password )
    {
        // By default, the combination is not valid
        $valid = FALSE;
                
        // Query for the password for the specified username
        $qryResult = $this->db->select( "password", "Member",
                "username='" . $this->db->escape( $username ) . "'" );
        
        // If there was one row returned, check the password against
        // the supplied password
        if ( $qryResult->num_rows == 1 )
        {
            $passwordRow = $qryResult->fetch_row();
            if ( $passwordRow[0] == password_verify( $password,
                                                     $passwordRow[0] ) )
            {
                $valid = true;
            }
        }
        
        // Return whether the username and password combination is valid
        return $valid;
    }

    
    public function addUser( $username, $password, $email)
    {
        // Create the array to use with the insert method
        $record["username"] = $this->db->escape( $username );
        $record["password"] = password_hash( $password, PASSWORD_DEFAULT );
        $record["email"] = $email;
        $record["privilege"] = PasswordChecker::PRIVILEGE_DEFAULT;        
        // Insert the user into the Password database
        $numRows = $this->db->insert( $record, "Member" );

        return ( $numRows == 1 );
    }    

    /**
     * Helper to see if an account exists already.
     * @param string $username To check the db if it exists
     * @return boolean False if username exists, true otherwise.
     */
    public function validUser($username)
    {
        $exists = true;
        $selectUser= "select username from Member where username='"
                . $username . "'";
        
        $userCheckResult = $this->db->runQuery($selectUser);
        if ($userCheckResult->num_rows > 0)
        {
            $exists=false;
        }
        return $exists;
    }
    
    /**
     * 
     * @param string $username The username to check the priv level of
     * @return int privilige level
     */
    public function displayPrivilegeLevel($username)
    {
        $stmt = "select privilege from Member where username='$username'";
        $result = $this->db->runQuery($stmt);
        $row = $result->fetch_row();
        $level = $row[0];
        return $level;
    }
}
