<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LO5: Retrieving data from a database with objects</title>
    </head>
    <body>
        <h1>LO5: Retrieving data from a database with objects</h1>
        <?php

$db = new dbObject();
echo "<p>Successfully connected to the database!</p>\n";

// Query the database
// $qryResults = $db->runQuery( "SELECT firstName, lastName FROM Instructor" );
$qryResults = $db->selectQuery( "firstName, lastName", "Instructor" );

// IF the query succeeded
if ( $qryResults ) {
    // Determine the number of rows returned and display it
    $numRows = $qryResults->num_rows;
    printf( "<p>Fetched %d rows from the Instructor table</p>\n", $numRows );
    
    // Display the records
    dbObject::displayRecords( $qryResults );
    
    // We're done with the results -- free them
    $qryResults->free();
} else {
    // Indicate that we failed to retrieve the results
    echo "<p><blink>Failed to fetch from the Instructor table</blink></p>\n";
}

        ?>

    </body>
</html>

<?php

function __autoload( $className )
{
    $fileName = $className . ".class.php";
    if (file_exists( "classes/$fileName" ) ) {
        require_once( "classes/$fileName" );        
    } else {
        require_once( "../../classes/$fileName" );
    }
}

?>

