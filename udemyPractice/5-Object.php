<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LO5: Retrieving data from a database with objects</title>
    </head>
    <body>
        <h1>LO5: Retrieving data from a database with objects</h1>
        <?php

//$db = new mysqli( "10.20.38.43", "shane", "hulk", "shane" );
$db = new mysqli( "10.20.32.217", "INS214", "RASSLER", "INS214" );
 if ( $db->connect_errno ) {
    echo "<p>Failed to connect to database: " . $db->connect_error .
            "</p>\n";
    exit();
} else {
    echo "<p>Successfully connected to the database!</p>\n";
}

// Query the database
$qryResults = $db->query( "SELECT firstName, lastName FROM Instructor" );

// IF the query succeeded
if ( $qryResults ) {
    // Determine the number of rows returned and display it
    $numRows = $qryResults->num_rows;
    printf( "<p>Fetched %d rows from the Instructor table</p>\n", $numRows );
    
    // instructorNum keeps track of which instructor is being displayed
    $instructorNum = 0;
    echo "<div>\n";

//    // LOOP while there are still instructors to be displayed
//    while ( $instructor = $qryResults->fetch_assoc() ) {        
//        // Display the instructor information
//        $instructorNum++;
//        printf( "Instructor %d: %s %s<br/>\n", $instructorNum,
//                $instructor["firstName"], $instructor["lastName"] );
//    }
    // LOOP while there are still instructors to be displayed
    while ( $instructor = $qryResults->fetch_array( MYSQLI_BOTH ) ) {        
        // Display the instructor information
        $instructorNum++;
        printf( "Instructor %d: %s %s<br/>\n", $instructorNum,
                $instructor[0], $instructor["lastName"] );
    }
    echo "</div>\n";
    
//    // LOOP while there are still instructors to be displayed
//    for ( $instructorNum = 1; $instructorNum <= $numRows; $instructorNum++ ) {
//        // Get the information for the next row
//        $instructor = $qryResults->fetch_row();
//        
//        // Display the instructor information
//        printf( "Instructor %d: %s %s<br/>\n", $instructorNum,
//                $instructor[0], $instructor[1] );
//    }
    
    echo "<p>The following fields were selected:</p>\n";
    echo "<ul>\n";
    // LOOP for all fields that were selected
    foreach ( $qryResults->fetch_fields() as $fieldInfo ) {
        // Display the name of the field
        echo "<li>{$fieldInfo->name}</li>\n";
    }    
    echo "</ul>\n";
    
    // We're done with the results -- free them
    $qryResults->free();
} else {
    // Indicate that we failed to retrieve the results
    echo "<p><blink>Failed to fetch from the Instructor table</blink></p>\n";
}

$db->close();

        ?>
    </body>
</html>
