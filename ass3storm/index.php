<?php 
session_start();
require 'Common.php';
function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>   
        <link href="form-table.css" rel="stylesheet" type="text/css"/>        
        
    </head>
    <body>
        
        <?php
        // db for displaying table
        $db = new DbObject("localhost", "root", "X41Ak7123", "assignment3");
        
        
        
        if (isset($_SESSION['userName']))
        {
            $pntrUser = $_SESSION['userName'];

            if (isset($_SESSION["loggedIn"]))
            {
                if ($_SESSION["loggedIn"])
                {
                    
                    $privLevel = $_SESSION["privLevel"];
                    switch ($privLevel) {
                        case 1:
                        {
                            echo displaySessionInfo($pntrUser);
                            echo "<h1 class='welcome'>Welcome, $pntrUser</h1>";
                            echo "<div class='accountInfo'><h2>Account Pending</h2>";
                            echo "<p>Your account has not yet been approved."
                            . "</p></div>";
                            break;
                        }
                        
                        case 2: {
                            
                            echo displaySessionInfo($pntrUser);
                            echo "<h1 class='welcome'>Welcome, $pntrUser</h1>";
                            $result = $db->runQuery("select username as 'Name', "
                                    . "email as 'Email' from member order by "
                                    . "username, email;");
                            echo "<div id='tableContainer'>";
                            $db->displayRecords($result);
                            echo "</div>";
                            break;  
                        }
                        
                        case 3:
                            echo displaySessionInfo($pntrUser);
                            echo "<h1 class='welcome'>Welcome, $pntrUser</h1>";
                            $result = $db->runQuery("select username as 'Name', "
                                    . "email as 'Email', privilege as 'Privilege'"
                                    . " from member order by "
                                    . "privilege, username, email;");
                            echo "<div id='tableContainer'>";
                            $db->displayRecords($result);
                            echo "</div>";
                            break; 
                    }                    
                }
            }            
        }            

        else {
        
        echo "<p><a href='register.php'>Register</a></p>";
        echo "<p><a href='login.php'>Login</a></p>";

        }
        ?>
    </body>
</html>
