<?php
session_start(); // start session
//
// load support classes

if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"]) {


    //header("HTTP/1.1 301 Moved Permanently");

    //header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);

    //exit();
} 
if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"]) {
    header("Location: index.php");
}

require('Common.php');
function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");
?>

<!DOCTYPE html>
<!--
Jesse Preiner
CST221
CWEB280Assignment3
Shane McDonald && Ernesto Basoalto
November 13 2014
-->
<html>
    <head>
        <link href="form-table.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>

        <meta charset="UTF-8">
        <title>Register now!</title>
    </head>
    <body>
        <?php
        // if the form hasn't yet been submitted
        if (!isset($_POST['registerSubmit'])) {
            // initialize form
            $registerForm = new HtmlForm();
            $registerForm->renderStart("registerForm", 
                    "Register for the CST Membership System");

            // render controls
            $registerForm->renderTextbox("username", "Username", true);
            $registerForm->renderTextbox("email", "Email address", true);
            $registerForm->renderPassword("password", "Password", true);
            $registerForm->renderPassword("retype", "Re-type", true);
            $registerForm->renderSubmitEnd("registerSubmit", "Register", "Clear");
            $registerForm->renderEnd();
        }
        // has been submitted, lets check it out
        else {


            /*
             * will either create an account for them, log them in, and automatically 
             * redirect them to the index.php page, or will provide a message 
             * indicating that either the username is already in use, that the 
             * passwords didn’t match, or that they didn’t specify a field, and will 
             * allow them to try registering again.
             */

            // if all fields are filled out 
            if (isset($_POST["password"]) 
                    && isset($_POST["retype"]) 
                    && isset($_POST["username"]) 
                    && isset($_POST["email"]) 
                    && strlen($_POST["username"]) > 3  // username long enough
                    && strlen($_POST["password"]) > 6) { // 6 char password
                // if passwords don't match
                if ($_POST["password"] != $_POST["retype"]) {

                    echo "<p>Oops! The verify re-type password field didn't match your"
                    . "password. Please try again.</p>";
                    echo "<p><a href='#' onclick='history.back(); return false;'>Click";
                    echo "    here</a> to try again</p>";

                    // passwords match, moving on
                } else {

                    $passwordChecker = new PasswordChecker();

                    // get user information
                    $user = $_POST["username"];
                    $pwd = $_POST["password"];
                    $email = $_POST["email"];

                    // if the username is unique
                    if ($passwordChecker->validUser(strip_tags($user))) {
                        // username will already be stripped of tags
                        $success = $passwordChecker->addUser($user, $email, $pwd);

                        // user successfully added, let them know.
                        if ($success) {
                            
                            echo "<p>The user $user has been added</p>";
                            echo "<p><a href='7-AddLogin.php'>Click here</a>\n";
                            echo "    to add another</p>\n";

                            // wasn't added
                        } else {
                            displayError("<p>Error adding $user as a member</p>");
                        }
                    }

                    // otherwise let `em have it
                    else {
                        displayError("<div class='error'><p>Error, user $user already exists!</p>");
                    }
                }
            } else {
                displayError("<div class='error'>Oops. Some of the fields weren't filled out.");
            }
        }

//        function displayError($message) {
//            echo "<p>$message</p>\n";
//            echo "<p><a href='#' onclick='history.back(); return false;' style='display:inline'>Click";
//            echo "    here</a> to try again</p></div>";
//        }
        ?>
           
        <div>    
            <p class='extraInfo'>
                Already have an account? Click <a href='login.php'>here</a> to
                log in.
            </p>
        </div>  
            
    </body>
</html>
