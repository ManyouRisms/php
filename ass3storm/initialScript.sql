use assignment3;
DROP TABLE IF EXISTS assignment3.Member;

CREATE TABLE assignment3.Member (
  memberID INT UNSIGNED NOT NULL AUTO_INCREMENT,
  username VARCHAR(20) NOT NULL,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(255) NOT NULL,
  privilege INT UNSIGNED NOT NULL,
  PRIMARY KEY (memberID) ) ENGINE=InnoDB;

INSERT INTO Member ( username, email, privilege, password )
VALUES ( 'admin', 'admin@admin.com', 3,
  '$2y$10$m0YFmrc1l7tHsBp0AWDwuerLgXT/WhXINpsOCA.GYisrefMLFaHO2' );

show tables;
select database();

use assignment3;

select * from Member;
INSERT INTO Member ( username,email,password,privilege ) VALUES ( 'jenn','jenn','1234','1' );
INSERT INTO Member ( username,email,password ) VALUES ( 'ag','grfsd','hd' );
INSERT INTO Member ( username,email,password,privilege ) VALUES ( 'jesse','jesse','1234','1' );

