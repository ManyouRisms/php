<?php
/**
 * VERSION: 3.0
 * Description of HtmlForm
 * this class will define HELPER methods to generate HTML forms on webpage
 * we will continue using this class going forward
 * to speed up coding of examples and assignments
 * IMPORTANT - DO NOT put a PHP closing tag in this file
 * @author cst2##
 */
class HtmlForm {
    
    
/************************* FORM TEMPLATE CONSTANTS ****************************/
    // define constants that act as templates for the sprintf and printf methods
    // we are using the HEREDOC to define strings that output formatted html
    const FORMSTART = <<<EOT
    <form id="%s" action="%s" method="%s" %s>
        <fieldset>
            <legend>%s</legend>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    
    //string constant wrap the inputs with a div and add label
    const INPUTWRAPPER = <<<EOT

            <div>
                <label for="%s">%s</label>
                %s
            </div>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    
    
    //string constant for rendering a submit and reset button on the page
    const FORMSUBMIT = <<<EOT

            <div>
                <input type="submit" value="%s" name="%s" />
                <input type="reset" value="%s" />
            </div>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    
    //string constant to close off the fieldset and the form
    const FORMEND = <<<EOT

        </fieldset>
    </form>

EOT;
// the above line indicates the end of string - nothing else can be on that line    
    
 
/********************** RENDER FORM TAGS *************************************/
    
    /**
     * 
     * @param string $name - the id of the form element
     * @param string $labelTextText - is the legend of the fieldset
     * @param bool $allowFile - if true specifies enctype="multipart/form-data" for the form
     * @param string $action - the page to post to (default: #)
     * @param string $method - the way data is sent to the server (default: POST);
     */
    public function renderStart($name, $labelTextText, $allowFile=false, 
            $action="#", $method="POST")
    {
        
        $enctype = $allowFile ? 'enctype="multipart/form-data"' : '';
        // to access a constant variable in php class use self::CONSTNAME
        printf(self::FORMSTART,$name,$action,$method,$enctype,$labelTextText);
    }
    
    
    
    /**
     * renderSubmitReset - generates submit and reset buttons
     * @param string $nameSubmit - the name of the submit button sent to the server
     * @param string $labelTextSubmit - the value attribute of the submit button
     *  - displayed to the user
     * @param string $labelTextReset - the value attribute of the reset button
     * - displayed to the user  (default: Reset)
     */
    public function renderSubmitReset($nameSubmit, $labelTextSubmit, $labelTextReset="Reset")
    {
        printf(self::FORMSUBMIT, $labelTextSubmit, $nameSubmit, $labelTextReset);
    }
        
    
    /**
     * renderSubmitEnd - generates the closing form tags and buttons
     * @param string $nameSubmit - the name of the submit button sent to the server
     * @param string $labelTextSubmit - the value attribute of the submit button
     *  - displayed to the user
     * @param string $labelTextReset - the value attribute of the reset button
     *  - displayed to the user  (default: Reset)
     */
    public function renderSubmitEnd($nameSubmit, $labelTextSubmit, $labelTextReset="Reset")
    {
        $this->renderSubmitReset($nameSubmit, $labelTextSubmit, $labelTextReset);
        $this->renderEnd();
    }
    
    
    /*
     * renderEnd - - generates the closing fieldset and form tags
     */
    public function renderEnd()
    {
        // to access a constant variable in php class use self::CONSTNAME
        echo self::FORMEND;
    }

    
/********************** CONSTRUCTOR AND HELPERS *******************************/
    
// private variable that stores whether the form was posted or not
    private $isPosted; //set this in the constructor  
    
    /**
     * constructor -  checks if the form was posted and set the isPosted variable
     */
    function __construct() {
        
        $this->isPosted = false;
        if(strtoupper($_SERVER["REQUEST_METHOD"]) == "POST")
        {
            $this->isPosted = true;
        }
        
    }
    
     /**
     * postedValue - will try to get the posted value for a field from
     * the $_POST super global
     * if the posted value is not set then the function with return a 
     * default value specified or null by default
     * @param string $name
     * @param object $defaultValue
     * @return string
     */
    private function postedValue($name, $escaped=true, $defaultValue=null)
    {
        //set the return value to the passed in default value
        $postedValue = $defaultValue;
        
        //check to see if the posted value is set and not empty
        if(!empty($_POST[$name]))
        {
            //we should filter the posted value to prevent code injection from the user
            //in some cases we may not want this function to filter the posted value so it is an option
            $postedValue = $escaped ? htmlentities($_POST[$name]):$_POST[$name];
        }
        return $postedValue;        
    }
    
    
    /*
     * checkRequired - generates an indicator that the field is requires
     * if the form has been posted this method will also check and generate
     * an appropriate error message if required
     */
    private function checkRequired($name, $labelText,
            $msgFormat='<span class="fielderror">* %s is required</span>')
    {
        
        $requiredMsg = "*"; //indicated that the field is required to the user
        //check if the form has been posted 
        
        if($this->isPosted)
        {
            // check that the posted value is not empty
            // trim the posted value to ensure the user did not post whitespaces
            if( !isset($_POST[$name]) || 
                    ( !is_array($_POST[$name]) && empty(trim($_POST[$name])) ) )
            {
            
                $requiredMsg = sprintf($msgFormat,$labelText);
            }
        }
        return $requiredMsg;
    }

/******************************* TEXTBOX **************************************/
    
    /**
     * renderTextbox - generates a wrapped text input
     * @param string $name - the name send to the server and the id of the element
     * @param string $labelText - the label of the field
     * @param bool  $required -  whether the field is required or not
     * @param int $max - maxlength of the  text box (default: 50)
     * @param string $value - the default value populated in the text box (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function renderTextbox($name, $labelText, $required=false, $max=50,
            $defaultValue="", $extras="")
    {
        
        //generate require field indicator or required error message
        $requiredField = $required ? $this->checkRequired($name, $labelText) : "";
        
        //get the escaped/filtered posted value to display to the user again
        $fieldValue=$this->postedValue($name,true,$defaultValue);
        
        //generate the textbox input tag
        $inputField = sprintf(
        '<input type="text" name="%s" id="%s" maxlength="%d" value="%s" %s />'
        ,$name,$name,$max,$fieldValue,$extras);
        
        //wrap the input tage with our layout (within a div and preceded by a label)
        //append the required field indicator/error message after the input tag
        //use printf to output all the html for the field
        printf(self::INPUTWRAPPER, $name, $labelText, 
                $inputField . $requiredField);
    }
    
    /**
     * renderPassword - generates a wrapped password input
     * @param string $name - the name sent to the server and the id of the element
     * @param string $labelText - the label of the field
     * @param int $max - maxlength of the  text box
     * @param string $defaultValue - the default value populated in the file input (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function renderPassword($name, $labelText, $required=false, $max=16,
            $defaultValue="", $extras="")
    {
        //generate require field indicator or required error message
        $requiredField = $required ? $this->checkRequired($name, $labelText) : "";
        
        // get the escaped/filtered posted value to display to the user again
        $fieldValue = $this->postedValue($name,true,$defaultValue);
        
        // generate the password input tag
        $inputField = sprintf('<input type="password" name="%s" id="%s" maxlength="%d" value="%s" %s />',
                $name, $name, $max, htmlentities($fieldValue), $extras);
        
        // wrap the input tag with our layout (within a div and preceded by a label)
        // append the required field indicator / error message after the input tag
        // use printf to output all the HTML for the field
        printf(self::INPUTWRAPPER, $name, $labelText,
                $inputField . $requiredField );
    }
    
/**************************** SELECT BOX **************************************/    
    /**
     * renderSelect - generates a form select box
     * @param string $name - the name and id of the select box
     * @param string $labelText -  the label of the select box
     * @param array $options - array of option values and text
     * @param string $selectedValue - value of the default option to be selected (default: empty string)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function renderSelect($name, $labelText, $options, 
            $selectedValue="", $extras="")
    {
        //select box is more comples than simple input tag
        //set the select format string 
        $selectFormat = <<<EOT
<select name="%s" id="%s" %s >%s
                </select>
EOT;
        
        //get the posted value for the field
        //do not escape the value because we are going to use it in a comparison
        $fieldValue=$this->postedValue($name,false,$selectedValue);
        
        //loop through the options array
        $optionsHTML = "";//create variable to contain all generated option html  
        foreach($options as $optionValue=>$labelTextText)
        {
            //check if the current option value is the same as the
            //posted value / selected value
            $selected = $fieldValue == $optionValue;
            
            //append the option tag to the string (.= shorthand for append to string)
            $optionsHTML .= $this->renderOption($optionValue, $labelTextText, $selected);
        }        
        
        $inputField = sprintf($selectFormat,$name, $name, $extras, $optionsHTML);
        printf(self::INPUTWRAPPER, $name, $labelText, $inputField);
        
    }
 
    /**
     * renderOption - renders a single option tag for use within a select tag
     * @param string $labelTextText - the option displayed text
     * @param string $optionValue - the option value
     * @param bool $selected - boolean specify the option as selected
     * @return type
     */
    private function renderOption($optionValue, $labelTextText, $selected)
    {
        //if the option is selected output the "selected" attribute
        $selected = $selected ? 'selected' : '';
        $optionFormat = <<<EOT

                    <option value="%s" %s >%s</option>
EOT;
        
        //generate the option tag - escape characters that may break option tag
        return sprintf($optionFormat, htmlentities($optionValue), $selected, 
                $labelTextText);
    }
    
    /**
     * renderMultiSelect - generates a form select box
     * @param string $name - the name and id of the select box
     * @param string $labelText -  the label of the select box
     * @param array $options - array of option values and text
     * @param array $selectedValues - array of values of the options to be selected (default: null)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function renderMultiSelect($name, $labelText, $options, 
            $selectedValues=null, $extras="")
    {
        //select box is more comples than simple input tag
        //set the select format string 
        $selectFormat = <<<EOT
<select name="%s[]" id="%s" multiple %s >%s
                </select>
EOT;
        
        //ensure that selectedValues is an array
        // if not an array set to an empty array
        $selectedValues = !is_array($selectedValues) ? array() : $selectedValues;
        
        //get the posted value for the field
        //do not escape the value because we are expecting and array instead of a string
        $fieldValues=$this->postedValue($name,false,$selectedValues);
        
        //loop through the options array
        $optionsHTML = "";//create variable to contain all generated option html  
        foreach($options as $optionValue=>$labelTextText)
        {
            //check if the current option value is the same as the
            //posted values / selected values
            $selected = in_array($optionValue, $fieldValues);
            
            //append the option tag to the string (.= shorthand for append to string)
            $optionsHTML .= $this->renderOption($optionValue, $labelTextText, $selected);
        }        
        
        $inputField = sprintf($selectFormat,$name, $name, $extras, $optionsHTML);
        printf(self::INPUTWRAPPER, $name, $labelText, $inputField);
        
    }    
       
/************************* RADIO BUTTON GROUP *********************************/
    
    
        /**
     * renderInput - generates a single form input markup for a input group
     * @param bool $type - the type of form input (default: radio)
     * @param string $name - the name of the input group (also used to generate a unique id)
     * @param string $labelText - the label of the form input
     * @param string $value - the form input value (also used to generate a unique id)
     * @param bool $checked - whether to mark the current form input as checked
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     * @return string
     */
    private function renderGroupItem($type, $name, $labelText, $value, $checked, $extras="")
    {
       //determine whether to mark the current form input as checked 
       $checked = $checked ? 'checked' : '';
       
       //create a unique id from the name and cleaned value of the form input
       //the id is used by label adjacent to the form input
       $id = preg_replace("/\W/i", "" , $name.$value);
       
       return sprintf('<div><input type="%s" name="%s" id="%s" value="%s" %s %s /><label for="%s">%s</label></div>
',$type, $name, $id, htmlentities($value), $checked, $extras, $id, $labelText);
       
    }    


    /**
     * renderRadioGroup - generates a wrapped group of radio buttons
     * @param type $name - the name of the radio group
     * @param type $labelText - the label for the radio Group
     * @param type $options - associative array of radio values and labels
     * @param bool  $required -  whether the field is required or not
     * @param type $selectedValue - the value of the default radio to be checked (default: empty string)
     * @param type $extras - any other attributes that maybe needed (default: empty string)
     */
    public function renderRadioGroup($name, $labelText, $options,  $required = false, $selectedValue="", $extras="")
    {
        //generate require field indicator or required error message
        $requiredField = $required ? $this->checkRequired($name, $labelText) : "";
        
        //get the posted value for the field
        //do not escape the value because we are going to use it in a comparison
        $fieldValue=$this->postedValue($name,false,$selectedValue);
        
        // surround the radio groups with a span tag
        $radioMarkUp = "<span>";
        
        //loop through the options array and call the option method
        foreach($options as $valueRadio=>$labelTextRadio)
        {
            //check if the current option value is the same as the
            //posted value / selected value
            $selected = $fieldValue == $valueRadio;
            
            //append the radio button string to the markup
            $radioMarkUp .= $this->renderGroupItem("radio", $name,
                    $labelTextRadio, $valueRadio, 
                    $selected, $extras);
        }
        
        $radioMarkUp .= "</span>";
        
        // no need to specify the name this time since each radio button will have its own label
        printf(self::INPUTWRAPPER, "", $labelText, $requiredField.$radioMarkUp);

    }
	
/************************** CHECK BOX GROUP ***********************************/	

    /**
     * renderCheckboxGroup - generates a wrapped group of checkboxes
     * @param string $name - the name of the checkbox group
     * @param string $labelText - the label for the checkbox Group
     * @param string $options - associative array of checkbox values and labels
     * @param bool  $required -  whether the field is required or not
     * @param array $selectedValues - array of values of the options to be selected (default: null)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function renderCheckboxGroup($name, $labelText, $options, $required = false, $selectedValues=null, $extras="")
    {
        //generate require field indicator or required error message
        $requiredField = $required ? $this->checkRequired($name, $labelText) : "";
        
        //ensure that selectedValues is an array
        // if not an array set to an empty array
        $selectedValues = !is_array($selectedValues) ? array() : $selectedValues;
        
        //get the posted value for the field
        //do not escape the value because we are expecting and array instead of a string
        $fieldValues=$this->postedValue($name,false,$selectedValues);
        
        // surround the radio groups with a span tag
        $checkboxMarkUp = "<span>";
        
        //adding '[]' to the name will indicate to php to save the multiple posted values in an array
        $name .='[]';
        
        //loop through the options array and call the option method
        foreach($options as $valueCheckbox=>$labelTextCheckbox)
        {
            //check if the current option value is the same as the
            //posted values / selected values
            $selected = in_array($valueCheckbox, $fieldValues);
            
            //append the checkbox string to the markup
            $checkboxMarkUp .= $this->renderGroupItem("checkbox", $name,
                    $labelTextCheckbox, $valueCheckbox, $selected, $extras);
        }
        
        $checkboxMarkUp .= "</span>";
        
        // no need to specify the name this time since each check box will have its own label
        printf(self::INPUTWRAPPER, "", $labelText, $requiredField.$checkboxMarkUp);

    }
 

    /**
     * renderCheckbox - generates a wrapped checkbox input
     * @param string $name - the name of the checkbox
     * @param type $labelText - the label of the checkbox
     * @param type $value - the default value of the checkbox (default: 1)
     * @param type $selected - whether to mark the checkbox as checked
     *   (default: false)
     * @param type $extras - any other attributes that may be required
     *   (default: empty string)
     */
    public function renderCheckbox( $name, $labelText, $value="1",
            $selected=false, $extras="" )
    {
        $selected = $this->postedValue( $name, true, $selected ) ?
                "checked" : "";
        $checkboxMarkUp = $this->renderGroupItem( "checkbox", $name,
                $labelText, $value, $selected, $extras);
        echo $checkboxMarkUp;
    }
    
    /**
     * renderHidden - generates a hidden form input
     * @param string $name - the name send to the server and the id of the element
     * @param string $value - the default value populated in the file input (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function renderHidden($name, $value="", $extras="")
    {
        $value = $this->postedValue($name,$value);
        //hidden inputs are not visible to the user so no need for the Input wrapper
        printf('<input type="hidden" name="%s" id="%s" value="%s" %s />',
                $name, $name,  htmlentities($value), $extras);        
    }    
}
