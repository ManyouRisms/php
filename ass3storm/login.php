<?php
session_start();

//if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"]) {


    //header("HTTP/1.1 301 Moved Permanently");

    //header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);

    //exit();
    
    
//} 
require('Common.php');
function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link href="form-table.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        
        if (isset($_GET["logout"]) && $_GET["logout"] )
        {
            unset($_SESSION["loggedIn"]);
            $_GET['logout']= false;

            session_destroy();
        }

        if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"]) {

        }        
        // if user is already logged in
            // redirect to index.php
        if (isset($_POST["submitLogin"])) {
            
            //if (isset($_POST["userName"]) && isset($_POST["password"])) {
            if (!empty($_POST["userName"]) && !empty($_POST["password"])) {
                
                    // submitted but not logged in
                    $passwordChecker = new PasswordChecker();
                    $username = $_POST["userName"];
                    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

                    //if ($passwordChecker->isValid($username, $password)) {
                    if (password_verify($_POST['password'], $password)) {
                    
                        $_SESSION["loggedIn"] = true;
                        $_SESSION["userName"] = $_POST["userName"];
                        
                        // store current user's privilege level
                        $_SESSION["privLevel"] = 
                                $passwordChecker->displayPrivilegeLevel($username);

                        header("Location: index.php");
                    }
                    else {
                        
                        displayError("Invalid username or password");
                    }
                
            }
            else
            {
                displayError("<div class='error'>Oops. Some of the fields"
                        . " weren't filled out.");
            }

        // present form for username, password, login, and ask if no account and
        //provide link (if not logged in)
        }
        else {
                
        $loginForm = new HtmlForm();
        $loginForm->renderStart("loginForm", "Login");
        
        // controls
        $loginForm->renderTextbox("userName", "Username");
        $loginForm->renderPassword("password", "Password");
        $loginForm->renderSubmitEnd("submitLogin", "Login");
        }
        
        ?>
        <div>    
            <p class='extraInfo'>
                Don't have an account? Click <a href='register.php'>here</a> to
                register.
            </p>
        </div>           
    </body>
</html>
