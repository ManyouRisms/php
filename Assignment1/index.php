<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
require './HtmlForm.class.php';
include './commonArraycode.php'; 
$form1 = new HtmlForm();
$form1->renderStart("Form", "Provide information");

?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="formstyles.css" />
        <meta charset="UTF-8">
        <title>Assignment1</title>
    </head>
    <body>
        
        <?php 
            $form1->renderCheckboxes("Choose", "servername", $myArray, true);
            $form1->renderTextbox("firstname", "First Name", true);
            $form1->renderRadioGroup("Sex","Sex", $optionsArray, true);
            $form1->renderMultiSelect("multibox", "labeltext", $myArray, true);
            $form1->renderSubmitEnd("testpost", "Test your html class");          
        ?>
        
    </body>
</html>
