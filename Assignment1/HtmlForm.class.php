<?php

/**
 * VERSION: 2.0
 * Description of HtmlForm
 * this class will define HELPER methods to generate HTML forms on webpage
 * we will continue using this class going forward
 * to speed up coding of examples and assignments
 * IMPORTANT - DO NOT put a PHP closing tag in this file
 * @author cst2##
 */
class HtmlForm {
    /*     * *********************** FORM TEMPLATE CONSTANTS *************************** */

    // define constants that act as templates for the sprintf and printf methods
    // we are using the HEREDOC to define strings that output formatted html
    const FORMSTART = <<<EOT
    <form id="%s" action="%s" method="%s" %s>
        <fieldset>
            <legend>%s</legend>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    //string constant wrap the inputs with a div and add label
    const INPUTWRAPPER = <<<EOT

            <div class="wrapper">
                <label for="%s" class="inputwrapper">%s</label>
                %s
            </div>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    //string constant for rendering a submit and reset button on the page
    const FORMSUBMIT = <<<EOT

            <div>
                <input type="submit" value="%s" name="%s" />
                <input type="reset" value="%s" />
            </div>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    //string constant to close off the fieldset and the form
    const FORMEND = <<<EOT

        </fieldset>
    </form>

EOT;

// the above line indicates the end of string - nothing else can be on that line    

    /**
     * Jesse's consts <input type="radio" name="%s" id="%s" value="%s"><label for="%s">%s</label>   
     */
    const ADDRADIOGROUP = <<<EOT
                    <label for="%s" class="radios"><input type="radio" name="%s" id="%s" value="%s" %s>%s</label>   
EOT;

    /*     * ******************** RENDER FORM TAGS ************************************ */

    /**
     * 
     * @param string $name - the id of the fomr element
     * @param string $displayText - is the legend of the fieldset
     * @param bool $allowFile - if true specifies enctype="multipart/form-data" for the form
     * @param string $action - the page to post to (default: #)
     * @param string $method - the way data is sent to the server (default: POST);
     */
    public function renderStart($name, $displayText, $allowFile = false, $action = "#", $method = "POST") {

        $enctype = $allowFile ? 'enctype="multipart/form-data"' : '';
        // to access a constant varaible in php class use self::CONSTNAME
        printf(self::FORMSTART, $name, $action, $method, $enctype, $displayText);
    }

    /**
     * renderSubmitReset - generates submit and reset buttons
     * @param string $nameSubmit - the name of the submit button sent to the server
     * @param string $displaySubmit - the value attribute of the submit button
     *  - displayed to the user
     * @param string $displayReset - the value attribute of the reset button
     * - displayed to the user  (default: Reset)
     */
    public function renderSubmitReset($nameSubmit, $displaySubmit, $displayReset = "Reset") {
        printf(self::FORMSUBMIT, $displaySubmit, $nameSubmit, $displayReset);
    }

    /**
     * renderSubmitEnd - generates the closing form tags and buttons
     * @param string $nameSubmit - the name of the submit button sent to the server
     * @param string $displaySubmit - the value attribute of the submit button
     *  - displayed to the user
     * @param string $displayReset - the value attribute of the reset button
     *  - displayed to the user  (default: Reset)
     */
    public function renderSubmitEnd($nameSubmit, $displaySubmit, $displayReset = "Reset") {
        $this->renderSubmitReset($nameSubmit, $displaySubmit, $displayReset);
        $this->renderEnd();
    }

    /*
     * renderEnd - - generates the closeing feildset and form tags
     */

    public function renderEnd() {
        // to access a constant varaible in php class use self::CONSTNAME
        echo self::FORMEND;
    }

    /*     * ******************** CONSTRUCTOR AND HELPERS ****************************** */

// private variable that stores whether the form was posted or not
    private $isPosted; //set this in the constructor  

    /**
     * constructor -  checks if the form was posted and set the isPosted variable
     */

    function __construct() {

        $this->isPosted = false;
        if (strtoupper($_SERVER["REQUEST_METHOD"]) == "POST") {
            $this->isPosted = true;
        }
    }

    /**
     * postedValue - will try to get the posted value for a field from
     * the $_POST super global
     * if the posted value is not set then the functon with return a 
     * default value specified or null by default
     * @param string $name
     * @param object $defaultValue
     * @return string 
     */
    private function postedValue($name, $escaped = true, $defaultValue = null) {
        //set the return value to the passed in default value
        $postedValue = $defaultValue;

        //check to see if the posted value is set and not empty
        if (!empty($_POST[$name])) {
            //we should filter the posted value to prevent code injection from the user
            //in some cases we may not want this function to filter the posted value so it is an option
            $postedValue = $escaped ? htmlentities($_POST[$name]) : $_POST[$name];
        }
        return $postedValue;
    }


    private function checkRequired($name, $labelText, $msgFormat = '<span class="fielderror">* %s is required. </span>') {
        $requiredMsg = "*";
        if (!isset($_POST[$name]) 
                && $this->isPosted             
                && empty($_POST[$name])) 
        
         {
            $requiredMsg = sprintf($msgFormat, $labelText);
        }
        return $requiredMsg;
    }

    /*     * ***************************** TEXTBOX ************************************* */

    /**
     * renderTextbox - generates a wrapped text input
     * @param string $name - the name send to the server and the id of the element
     * @param string $display - the label of the field
     * @param int $max - maxlength of the  text box (default: 50)
     * @param string $value - the default value populated in the text box (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function renderTextbox($name, $labelText, $required = false, $max = 50, $defaultValue = "", $extras = "") {

        //generate require field indicator or required error message
        $requiredField = $required ? $this->checkRequired($name, $labelText) : "";
        //get the escaped/filtered posted value to display to the user again
        $fieldValue = $this->postedValue($name, true, $defaultValue);
        //generate the textbox input tag
        $inputField = sprintf(
                '<input type="text" name="%s" id="%s" maxlength="%d" value="%s" %s />'
                , $name, $name, $max, $fieldValue, $extras);

        //wrap the input tage with our layout (within a div and preceded by a label)
        //append the required field indicator/error message after the input tag
        //use printf to output all the html for the field
        printf(self::INPUTWRAPPER, $name, $labelText, $inputField . $requiredField);
    }


    /*     * ************************** SELECT BOX ************************************* */

    /**
     * renderSelect - generates a form select box
     * @param string $name - the name and id of the select box
     * @param string $display -  the label of the selectbox
     * @param array $options - array of option values and text
     * @param string $selectedValue - value of the default option to be selected (default: empty string)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function renderSelect($name, $labelText, $options, $selectedValue = "", $extras = "") {
        //select box is more complex than simple input tag
        //set the select format string 
        $selectFormat = <<<EOT
<select name="%s" id="%s" %s >%s
                </select>
EOT;

        //get the posted value for the field
        //do not escape the value because we are going to use it in a comparison
        $fieldValue = $this->postedValue($name, false, $selectedValue);
        //loop through the options array
        $optionsHTML = ""; //create variable to contain all generated option html  
        foreach ($options as $optionValue => $displayText) {
            //check if the current option value is the same as the
            //posted value / selected value
            $selected = $fieldValue == $optionValue;

            //append the option tag to the string (.= shorthand for append to string)
            $optionsHTML .= $this->renderOption($optionValue, $displayText, $selected);
        }

        $inputField = sprintf($selectFormat, $name, $name, $extras, $optionsHTML);
        printf(self::INPUTWRAPPER, $name, $labelText, $inputField);
    }

    public function renderMultiSelect($name, $labelText, $options, $selectedValues = null, $extras = "") {
        //select box is more comples than simple input tag
        //set the select format string 
        $selectFormat = <<<EOT
<select name="%s[]" id="%s" multiple %s >%s
                </select>
EOT;
        //we need to ensure $selectedValues is an array
        //if it is not then set it to an empty array
        
        $selectedValues = !is_array($selectedValues) ? array() : $selectedValues;
        
        //get the posted value for the field
        //do not escape the value because we are expcting an array instead of a string
        $fieldValues = $this->postedValue($name, false, $selectedValues);

        //loop through the options array
        $optionsHTML = ""; //create variable to contain all generated option html  
        foreach ($options as $optionValue => $displayText) {
            //check if the current option value is the same as the
            //posted value / selected value
            $selected = in_array($optionValue, $fieldValues);

            //append the option tag to the string (.= shorthand for append to string)
            $optionsHTML .= $this->renderOption($optionValue, $displayText, $selected);
        }

        $inputField = sprintf($selectFormat, $name, $name, $extras, $optionsHTML);
        printf(self::INPUTWRAPPER, $name, $labelText, $inputField);
    }

    /**
     * renderOption - renders a single option tag for use within a select tag
     * @param string $displayText - the option displayed text
     * @param string $optionValue - the option value
     * @param bool $selected - boolean specify the option as selected
     * @return type
     */
    private function renderOption($optionValue, $displayText, $selected) {
        //if the option is selected output the "selected" attribute
        $selected = $selected ? 'selected' : '';
        $optionFormat = <<<EOT

                    <option value="%s" %s >%s</option>
EOT;

        //generate the option tag - escape chararactes that may break option tag
        return sprintf($optionFormat, htmlentities($optionValue), $selected, $displayText);
    }

    /*     * *********************** RADIO BUTTON GROUP ******************************** */

    /**
     * 
     * @global string $radioString Implementation detail
     * @param type $groupName Name of the radio group.
     * @param type $rdoServerName Name that the server will use for each radio.
     * @param type $aValues Array k=>v values to use for value/display text
     * @param type $required Display friendly message if field is required.
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function renderRadioGroup($groupName, $rdoServerName, $aValues, $required = false, $extras = "") {
        global $radioString;
        $selected = "";

        //display required message if necessary.
        $requiredMsg = $required ? $this->checkRequired($rdoServerName, $groupName) : "";

        // ensure array passed array has values        
        if ($aValues != null && count($aValues) > 0) {

            // loop through array and pull out information for labels and radios
            foreach ($aValues as $radioValue => $labelText) {
                //$fieldValue = $this->postedValue($name, true, $defaultValue);
               
                //echo "<div> $this->isPosted </div>";
                //echo "<div> $this->postedValue($rdoServerName) </div>";
                $selected = $this->isPosted && $this->postedValue($rdoServerName ) == $radioValue ? "checked" : "";

                //defining known ID to use with 'for' in label
                $id = sprintf("rdo%s", str_replace(' ', '', $labelText));

                //practice using the constant, send string off using values.
                $radioString .= sprintf(self::ADDRADIOGROUP, $id, $rdoServerName, $id, $radioValue, $selected . $extras, $labelText);
            }
            // pass constructed string to get wrapped in div
            printf(self::INPUTWRAPPER, "", $groupName, $radioString . $requiredMsg);
        }
    }

    /*     * ************************ CHECK BOX GROUP ********************************** */

    /**
     * 
     * @global string $checkString Implementation detail
     * @param type $groupName Name of the checkbox group.
     * @param type $chkServerName Name that the server will use for each checkbox.
     * @param type $aValues Array k=>v values to use for value/display text
     * @param type $required Display friendly message if field is required.
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function renderCheckboxes($groupName, $chkServerName, $aValues, $required = false, $extras = "") {
        global $checkString;
        $requiredMsg = $required ? $this->checkRequired($chkServerName, $groupName) : "";

        // ensure array passed array has values
        if ($aValues != null && count($aValues) > 0) {

            // practice using in-method string
            $string = '<label for="%s" class="checkboxes"><input type="checkbox" id="%s" name="%s[]" value="%s" %s/>%s</label>';

            // loop through array and pull out information for labels and boxes
            foreach ($aValues as $checkValue => $labelText) {
                $id = sprintf("chk%s", str_replace(' ', '', $labelText));
                
                // this->postedvalue returning an array
                $selected = ( in_array($checkValue, $this->postedValue($chkServerName, false) != null ? $this->postedValue($chkServerName, false)
                        : array())  ? "checked" : "");
                $checkString = $checkString . sprintf($string, $id, $id, $chkServerName, $checkValue, $selected . $extras, $labelText);
            }
            // pass constructed string to get wrapped in div
            printf(self::INPUTWRAPPER, "", $groupName, $checkString . $requiredMsg);
        }
    }

}
