<?php
require './HtmlForm.class.php';

/**
 * Description of Html5Form
 *
 * @author jesse
 */
class Html5Form extends HtmlForm 
{
    public function __construct() {
        parent::__construct();
    }
    
    
    function renderCheckboxes($groupName, $chkServerName, $aValues, $required = false) {
                $newExtra = "";
                $newExtra .= $required ? "required= required" : "";
        parent::renderCheckboxes($groupName, $chkServerName, $aValues, $required);
    }
    

    function renderRadioGroup($groupName, $rdoServerName, $aValues, $required = false) {
                $newExtra = "";
                $newExtra .= $required ? "required= required" : "";
        parent::renderRadioGroup($groupName, $rdoServerName, $aValues, $required, $newExtra);
    }
    
    // call parents constructor with some extras
    function renderTextbox($name, $labelText, $required = false, $max = 50, $defaultValue = "", $extras = "") {
        $newExtra = "";
        $newExtra .= $required ? "required= required" : "";
        parent::renderTextbox($name, $labelText, $required, $max, $defaultValue, $newExtra);
    }
}
