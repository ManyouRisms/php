<?php

    include './commonArraycode.php';
    include './Html5Form.class.php';
    

?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="formstyles.css" />
        <meta charset="UTF-8">
        <title>Assignment1 - Jesse Preiner</title>
    </head>
    <body>
        <div>

            <?php

            $form1 = new Html5Form();
            $form1->renderStart("Form", "Provide information");
            
            $form1->renderCheckboxes("Choose", "servername", $myArray, true);
            $form1->renderTextbox("firstname", "First Name", true);
            $form1->renderRadioGroup("Sex","Sex", $optionsArray, true);
            $form1->renderMultiSelect("multibox", "labeltext", $myArray, true);
            $form1->renderSubmitEnd("testpost", "Test your html class");  
            ?>

        </div>
    </body>
</html>
