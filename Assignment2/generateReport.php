<!DOCTYPE html>
<!-- 
Jesse Preiner : CST221
CWEB280 - Shane McDonald && Ernesto Basoalto
generateReport.php

October 23rd 2014
-->
<?php

function my_autoloader($className) {
    include("classes/$className.class.php");
}

spl_autoload_register("my_autoloader");

// create new form
$addCarForm = new HtmlForm();
$addCarForm->renderStart("carform", "Generate Report");
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Generate report</title>
        <link rel="stylesheet" type="text/css" href="reportstylesheet.css" />
        <script type="text/javascript" src='back.js'></script>
    </head>
    <body>
        <?php
        $dbObject = new DbObject();

        // template with placeholders for the query
        $queryTemplate = "select 
                plateNum as 'License Plate Number', 
                make as Make, 
                model as Model,
                year as 'Model Year',
                if (hasPermit=1, 'Yes', 'No') 
                    as 'Permit',
                concat(ucase(left(firstName,1)), lcase(substring(firstName, 2)), ' ', 
                ucase(left(lastName,1)), lcase(substring(lastName, 2))) as 'Owner Name',
                
                phoneNum as 'Phone Number',
                email as 'Email'
            from 
                cst221_car car join cst221_owner owner on car.ownerID = owner.ownerID
            %s %s;";

        // successful connection
        if ($dbObject) {

            // variables to base query on to get sort orders + columns
            $colName = ( isset($_GET['colName']) ? "order by $_GET[colName] " : "");
            $direction = ( isset($_GET['colName']) ? $_GET['sortDirection'] 
                    == "asc" ? "desc" : "asc" : "");

            // obtain the result string
            $results = sprintf($queryTemplate, $colName, $direction);
            // run the query and get resultset
            $qryResult = $dbObject->runQuery($results);

            // generate report
            $dbObject->generateReport($qryResult, $direction, $colName);

            echo "<div><button id='ownerBack' onclick='goBack(); return false;'>Back</button></div>";
        } else {
            //death
            die("Can't open");
        }
        ?>
    </body>
</html>
