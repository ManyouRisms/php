drop table if exists cst221_car;
drop table if exists cst221_owner;

-- owner table
CREATE TABLE cst221_owner 
(
  ownerID SMALLINT NOT NULL AUTO_INCREMENT,
  firstName varchar(16) NOT NULL,
  lastName varchar(24) NOT NULL,
  phoneNum varchar(11) NOT NULL,
  email varchar(45) NOT NULL,
  PRIMARY KEY (ownerID)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- car table foreign key ownerID references cst221_owner.ownerID
CREATE TABLE `cst221_car` 
(
  plateNum varchar(7) NOT NULL,
  make varchar(12) NOT NULL,
  model varchar(12) NOT NULL,
  year int(4) unsigned  NOT NULL,
  hasPermit tinyint(1) NOT NULL DEFAULT '0',
  ownerID SMALLINT DEFAULT NULL,
  CHECK ( year > 1899 ),
  PRIMARY KEY (plateNum),
  CONSTRAINT ownerID_FK
  FOREIGN KEY (ownerID)
  REFERENCES CST221.cst221_owner (ownerID)
  ON DELETE SET NULL
  ON UPDATE NO ACTION
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;

select * from cst221_owner;
select * from cst221_car;
