-- uncomment for some useful checks

-- select * from cst221_car;
-- select * from cst221_owner;
-- desc cst221_car;
-- desc cst221_owner;


-- some car data
insert into cst221_car
values ('1015y5', 'Chevrolet', 'Malibu', 2012, 0, null);

insert into cst221_car
values ('102isj', 'Ford', 'Fusion', 2012, 0, null);

insert into cst221_car
values ('104p1p', 'Toyota', 'Camary', 2002, 0, null);

insert into cst221_car
values ('103asd', 'Toyota', 'Corolla', 1998, 1, null);

insert into cst221_car
values ('108lol', 'Mazda', '3', 2012, 1, null);


-- some owner data
insert into cst221_owner
(firstName, lastName, phoneNum, email)
values ('darkwing', 'duck', 1234567, 'darkwingduck@tv.com');

insert into cst221_owner
(firstName, lastName, phoneNum, email)
values ('inspector', 'gadget', 5558008, 'igadget@disney.ca');

insert into cst221_owner
(firstName, lastName, phoneNum, email)
values ('brian', 'wilson', 5552302, 'admin@beachboys.com');

insert into cst221_owner
(firstName, lastName, phoneNum, email)
values ('jay', 'leno', 123-4567, 'thechin@latenight.tv');

-- assigning some owner id's now that we have some
insert into cst221_car
values ('999iso', 'Dodge', 'Viper', 2015, 1, 1);

insert into cst221_car
values ('108bzw', 'Ford', 'Escape', 2010, 1, 2);

	
