<?php

/**
 * The dbObject class represents a connection to a MySQL server.
 * With objects of this class, we'll be able to create and execute
 * query statements.  This is a convenience class, wrapped around
 * the mysqli object.
 *
 * @author ins214
 */
class DbObject {
    
    const QUERY_WRAPPER = <<<EOT
            
            select 
                plateNum as 'License Plate Number', 
                make as Make, 
                model as Model,
                year as 'Model Year',
                if (hasPermit=1, 'Yes', 'No') 
                    as 'Has Parking Permit?',
                concat(ucase(left(firstName,1)), lcase(substring(firstName, 2)), ' ', 
                ucase(left(lastName,1)), lcase(substring(lastName, 2))) as 'Owner Name',
                
                phoneNum as 'Phone Number',
                email as 'Email'
            from 
                cst221_car car join cst221_owner owner on car.ownerID = owner.ownerID
            %s %s;
EOT;
    
    
    /**
     * @var mysqli dbConnect The database connection
     */
    private $dbConnect;
    
    /**
     * Purpose: To create a connection to a MySQL server and
     *   open a database on that server
     * @param string $server - name of the MySQL server
     * @param string $user - name of the MySQL user
     * @param string $password - user's password
     * @param string $schema - name of the schema to use
     */
    public function __construct( $host="kelcstu06", $user="CST221", $password="ZRBFIA", $schema="CST221" )
    {
        // Create a mysqli object, and assign it to the internal attribute
        $this->dbConnect = @new mysqli( $server, $user, $password, $schema );

        // IF the connection failed
        if ( $this->dbConnect->connect_error )
        {
            // Display an error message
            // Exit
            echo "<p>Failed to connect to database: " .
                    $this->dbConnect->connect_error . "</p>\n";
            exit();
        }
    }
    
    /**
     * Purpose: Close the database connection.
     * The destructor gets called when the object goes out of scope
     * (function terminates, program ends, destroy object).  Rather than
     * having a separate close method (which might be a good idea anyways,
     * because then we can close early if we want), we'll just close
     * the connection here.
     */
    public function __destruct()
    {
        // Close the database connection
        $this->dbConnect->close();
    }
    
    /**
     * Purpose: This routine will run the query that is provided by the query
     *   string that is passed in.  If the query fails, we will stop processing
     *   and exit the PHP interpreter immediately.
     * @param string $qryString The SQL query string that is to be run
     * @return mysqli_result The result of the query if the query is
     *   SELECT, DESCRIBE, EXPLAIN, or SHOW, or TRUE otherwise.
     */
    public function runQuery( $qryString )
    {
        // Execute the query
        $qryResult = $this->dbConnect->query( $qryString );
        
        // IF the query failed
        if ( !$qryResult )
        {
            // Print an error message, then exit
            echo "Query $qryString couldn't execute\n";
            exit();
        }
        
         //echo "Running:<br />$qryString";
        // Return the result of the query
        return $qryResult;
    }
    
    /**
     * Purpose: Perform a SELECT query on the database
     * @param string $columnList List of columns to be selected
     * @param string $tableList List of tables to select from
     * @param string $condition Optional SQL condition to select with
     * @param string $sort Optional SQL sort statement to apply
     * @param string $other Optional any other SQL clauses to apply
     * @return mysqli_result The result of the SELECT query
     */
    public function select( $columnList, $tableList,
            $condition="", $sort="", $other="" )
    {
        // Create the basic SELECT statement
        $qryStmt = "SELECT $columnList FROM $tableList";
        
        // If a condition is specified, add it to the query
        if ( $condition != "" )
        {
            $qryStmt .=  " WHERE $condition";
        }
        
        // If a sort order is specified, add it to the query
        if ( $sort != "" )
        {
            $qryStmt .=  " ORDER BY $sort";
        }
        
        // Add any other SQL clauses if they've been specified
        if ( $other != "" )
        {
            $qryStmt .= " $other";
        }
        
        // Execute the query and return the result
        return $this->runQuery( $qryStmt );
    }
    
    public function deleteMe()
    {
        
        
    }
    
    
    
    
        /**
     * Purpose: Display the results of a database query in an HTML table
     * @param mysqli_result $qryResults Results of a previous database query
     */
    public function generateReport( &$qryResults, $direction="asc", $colToSort="plateNum")
    {   
        if (isset($_REQUEST['colName']))
        {
            $colName = $_REQUEST['colName'];

        }
        else
        {
            $colName="";
            $direction="";                    
        }
        
        
        $printString = "<table>\n<thead><tr>";
        
        // LOOP for all query result columns
        foreach ( $qryResults->fetch_fields() as $fieldInfo )
        {
            // Display the column name within a table header tag
            $printString .= "<th><a href=generateReport.php?colName={$fieldInfo->orgname}&sortDirection=$direction>{$fieldInfo->name} <a/></th>";
        }

        $printString .= "    </tr>\n </thead>\n<tbody>\n";        
        
        // LOOP for all the query rows returned
        while ( $row = $qryResults->fetch_row() )
        {
            // Display a table row opening tag
            $printString .= "    <tr>";
            // LOOP for all the query result columns
            for ( $i = 0; $i < $qryResults->field_count; $i++ )
            {
                // Display the value of this query result row and column
                // within a table data tag
                $printString .= "<td>{$row[$i]}</td>";
            }
            // Display a table row closing tag
            $printString .= "</tr>\n";
        }
        // Display the closing tbody tag and closing table tag
        $printString .= "  </tbody>\n</table>\n";

        $queryString = sprintf($printString,$colName,$direction);
        
        
        echo "<br />$queryString<br />";
    }
    
    
    /**
     * Purpose: Display the results of a database query in an HTML table
     * @param mysqli_result $qryResults Results of a previous database query
     */
    public static function displayRecords( $qryResults, $sortString="ASC ", $colToSort="0" )
    {
       // Display the opening table tag
        echo "<table>\n";
        
        // Display the opening thead tag
        echo "  <thead>\n";
        
        // Display a table row opening tag
        echo "    <tr>";
        
        // LOOP for all query result columns
        foreach ( $qryResults->fetch_fields() as $fieldInfo )
        {
            // Display the column name within a table header tag
            echo "<th><a href=generateReport.php?order=" . $sortString . ">{$fieldInfo->name}<a/></th>";
        }
        // Display a table row closing tag
        echo "    </tr>\n";
        
        // Display a thead closing tag and a tbody opening tag
        echo "  </thead>\n";
        echo "  <tbody>\n";
        
        // LOOP for all the query rows returned
        while ( $row = $qryResults->fetch_row() )
        {
            // Display a table row opening tag
            echo "    <tr>";
            // LOOP for all the query result columns
            for ( $i = 0; $i < $qryResults->field_count; $i++ )
            {
                // Display the value of this query result row and column
                // within a table data tag
                echo "<td>{$row[$i]}</td>";
            }
            // Display a table row closing tag
            echo "</tr>\n";
        }
        // Display the closing tbody tag and closing table tag
        echo "  </tbody>\n";
        echo "</table>\n";
    }
        /**
     * Purpose: Creates an associative array to be used with the GenerateForm
     *   class' methods that populate lists.
     * @param mysqli_result $qryResult The query result record set.  The
     *   result should consist of two columns: the first column will contain
     *   an ID, and second will contain corresponding text.
     * @return array An associative array, where the array index comes from
     *   the qryResult's first column, and the array value comes from the
     *   qryResult's second column.
     */
    static public function createArray( $qryResults ) {
        
        $result = array(); 
        while ( $row = $qryResults->fetch_row() ) {
            
            $result[$row[0]] = $row[1];
        }
        return $result;
    }
    
        //  initial run at an insert statement, not used, kept for memories.
        public function preparedInsert($params, $insertString=
            "INSERT INTO cst221_car VALUES (?, ?, ?, ?, ?,?)" )
        {
            $statement = $this->dbConnect->prepare($insertString);
        
            $statement->bind_param("sssiii", $plate, $make,$model,$year,
                                            $permit, $owner);

            if (!$statement->execute())
            {
                echo "<p class='error'> $statement->error ($statement->errno)</p>";
            }            
        }
        
        
     /**
     * This method will add a new record to a specified table
     * @param array $newRecord An associated array of field names and the 
     *  values to be inserted
     * @param string $tableName The name of the table to add the record to
     * @return int The number of rows inserted
     */
    public function insert($newRecord, $tableName) {
        // Construct the field name and value lists
        $fieldList = "( ";
        $valueList = "( ";

        foreach ($newRecord as $field => $value) {
            if ($field == 'year')
            {
                // if we're in the year field , make it an int and check it's value
                $value = intval($value);

                if ($value < 1900)
                {         
                    throw new InvalidArgumentException("Year should be "
                            . "greater than 1900. Received $value"  );
                }
            }
            $fieldList .= $field . ",";
            $valueList .= "'" . $this->dbConnect->real_escape_string($value) . "',";
        }

        // trim extra comma
        $fieldList = rtrim($fieldList, ",");
        $valueList = rtrim($valueList, ",");

        $fieldList .= " )";
        $valueList .= " )";



        //insert
        $insStatement = "INSERT INTO " . $tableName . " " . $fieldList .
                " VALUES " . $valueList;

        $this->runQuery($insStatement);

        return $this->dbConnect->affected_rows;
    }
}
