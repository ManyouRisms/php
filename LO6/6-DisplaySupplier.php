<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Update a Supplier</title>
    </head>
    <body>
        <h1>Update Supplier Information</h1>
        <div>
<?php

// Connect to the database
$db = new DbObject();

// Query the database
$qryResults = $db->select( "*", "Suppliers",
        "SupplierID=" . $_POST["SupplierID"] );

// There will only be one supplier selected.  Get the information for that
// supplier.
$supplierInfo = $qryResults->fetch_assoc();

// We're done with the results -- free them
$qryResults->free();

// Create the supplier information update form
$form = new HtmlForm();
$form->renderHidden("SupplierID", $supplierInfo['SupplierID']);
$form->renderStart( "supplierInfoForm",
        "Supplier being updated: " . $supplierInfo["CompanyName"], false,
        "6-UpdateSupplier.php", "POST" );

$form->renderHidden( "SupplierID", $supplierInfo["SupplierID"] );
$form->renderTextbox( "CompanyName", "Company name", false, 40,
        $supplierInfo["CompanyName"] );
$form->renderTextbox( "ContactName", "Contact name", false, 30,
        $supplierInfo["ContactName"] );
$form->renderTextbox( "ContactTitle", "Contact title", false, 40,
        $supplierInfo["ContactTitle"] );
$form->renderTextbox( "Address", "Address", false, 60,
        $supplierInfo["Address"] );
$form->renderTextbox( "City", "City", false, 15,
        $supplierInfo["City"] );
$form->renderTextbox( "Region", "Region", false, 15,
        $supplierInfo["Region"] );
$form->renderTextbox( "PostalCode", "Postal code", false, 10,
        $supplierInfo["PostalCode"] );
$form->renderTextbox( "Country", "Country", false, 15,
        $supplierInfo["Country"] );
$form->renderTextbox( "Phone", "Phone", false, 24,
        $supplierInfo["Phone"] );
$form->renderTextbox( "Fax", "Fax", false, 24,
        $supplierInfo["Fax"] );
$form->renderTextbox( "HomePage", "Home page", false, 50,
        $supplierInfo["HomePage"] );
$form->renderSubmitReset( "subSupplier", "Update supplier" );
$form->renderEnd();

?>
        </div>
    </body>
</html>
