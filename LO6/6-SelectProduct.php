<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Select products to delete from an order</title>
    </head>
    <body>
<?php

// IF an order ID has been specified
if ( isset( $_REQUEST["OrderID"] ) ) {
    // Display header information for the web page
    echo "<p>Select which line items to delete from order " .
            $_REQUEST["OrderID"] . "</p>\n";

    // Connect to the database
    $db = new DbObject();

    // Create a query, asking for a list of all product IDs and
    // product names associated with the specified order ID
    $qryResults = $db->select(
      "OrderDetails.ProductID, Products.ProductName",
   "OrderDetails JOIN Products ON Products.ProductID=OrderDetails.ProductID",
      "OrderID=" . $_REQUEST["OrderID"], "Products.ProductName" );

    // Build the option list of products
    $optionList = DbObject::createArray( $qryResults );

    // We're done with the results -- free them
    $qryResults->free();

    // Create the form to allow selection of items from the specified order.
    // Be sure to include the order ID!
    $form = new HtmlForm();
    $form->renderStart( "productForm", "Select line items",
            false, "6-DeleteProduct.php", "GET" );
    $form->renderHidden( "OrderID", $_REQUEST["OrderID"] );
    $form->renderSelect( "ProductID[]", "Products", $optionList, "",
            "multiple='multiple' size='6'" );
    $form->renderSubmitReset( "subProducts", "Delete" );
    $form->renderEnd();

} else {
    // Display a message indicating that an order ID must be specified
    echo "<h1>Error when deleting items from an order</h1>\n";
    echo "<p>An order ID must be specified</p>\n";
    echo "<p><a href='6-SelectOrder.php'>" .
            "Click here</a> to select an order</p>\n";
}

?>
    </body>
</html>
