<?php
function my_autoloader( $className )
{
    require_once( "../../classes-B/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO6-B-TestInsert</title>
    </head>
    <body>
        <h1>LO6-B-TestInsert</h1>
        <div>
<?php
// connect to the database
$db = new DbObject();

$newRecord["firstName"] = "Jason";
$newRecord["lastName"] = "Schmidt";
$newRecord["email"] = "jason.schmidt@saskpolytech.ca";
$newRecord["startDate"] = "2010-07-01";

$retVal = $db->insert( $newRecord, "Instructor" );

echo "insert method returned $retVal<br>\n";
?>
        </div>
    </body>
</html>
