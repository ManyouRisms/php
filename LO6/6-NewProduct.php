<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add a new product</title>
        <link rel="stylesheet" href="form.css">
    </head>
    <body>
        <h1>Add a new product</h1>
        <div>
<?php
// Connect to the database
$db = new DbObject();

// Query the database for the suppliers
$qryResults = $db->select( "SupplierID, CompanyName", "Suppliers",
        "", "CompanyName" );

// Display the records
// dbObject::displayRecords( $qryResults );

// Build the supplier option list
// $optionList = array();
// while ( $row = $qryResults->fetch_row() )
// {
//     $optionList[$row[0]] = $row[1];
// }
$supplierOptionList = dbObject::createArray( $qryResults );

// We're done with the results -- free them
$qryResults->free();

// var_dump( $optionList );

// Get the list of category IDs and names
$qryResults = $db->select( "CategoryID, CategoryName", "Categories", "",
        "CategoryName" );

// Build the category option list
$catOptionList = dbObject::createArray( $qryResults );

// Free the results
$qryResults->free();

$checkboxArray = array();
$checkboxArray["Discontinued"] = "Discontinued?";

$form = new HtmlForm();
$form->renderStart( "supplierForm", "Choose a supplier", false,
        "6-AddProduct.php", "POST" );
$form->renderTextbox( "ProductName", "Product name", true, 40 );
$form->renderSelect( "SupplierID", "Supplier", $supplierOptionList );
$form->renderSelect( "CategoryID", "Category", $catOptionList );
$form->renderTextbox( "QuantityPerUnit", "Quantity per unit", true, 20 );
$form->renderTextbox( "UnitPrice", "Unit price", true, 20 );
$form->renderTextbox( "UnitsInStock", "Units in stock", true, 6 );
$form->renderTextbox( "UnitsOnOrder", "Units on order", true, 6 );
$form->renderTextbox( "ReorderLevel", "Reorder level", true, 6 );
$form->renderCheckbox( "Discontinued", "Discontinued?", "Discontinued" );
$form->renderSubmitReset( "subSupplier", "Submit" );
$form->renderEnd();
?>
        </div>
    </body>
</html>
