<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>

<!-- 
Jesse Preiner : CST221
CWEB280 - Shane McDonald && Ernesto Basoalto
generateReport.php

October 23rd 2014
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>New product addition - Confirmation</title>
    </head>
    <body>
        <h1>New product addition</h1>
        <div>
<?php


if ( !isset( $_REQUEST["Discontinued"] ) )
{
    $_REQUEST["Discontinued"] = 0;
}

unset( $_REQUEST["subSupplier"] );


// Connect to the database
$db = new DbObject();

// Add the new product to the database
//$affectedRows = $db->insert( $_REQUEST, "Products" );

$qryStmt = "INSERT INTO Products ( ProductName, SupplierID, "
        . "CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, "
        . "UnitsOnOrder, ReorderLevel, Discontinued )"
        . "VALUES ( ????????? )" ;
echo $qryStmt;
$preparedStmt = $db->prepare($qryStmt);

$preparedStmt->bind_param("siisdiiii", $productName, $supplierID, $categoryID, 
        $quantityPerUnit, $unitPrice, $unitsInStock, $unitsOnOrder, 
        $reorderLevel, $discontinued);


$productName= $_REQUEST["ProductName"];
$supplierID= $_REQUEST["SupplierID"];
$categoryID= $_REQUEST["CategoryID"];
$quantityPerUnit= $_REQUEST["QuantityPerUnit"];
$unitPrice= $_REQUEST["UnitPrice"];
$unitsInStock= $_REQUEST["UnitsInStock"];
$unitsOnOrder= $_REQUEST["UnitsOnOrder"];
$reorderLevel= $_REQUEST["ReorderLevel"];
$discontinued= $_REQUEST["Discontinued"];


$preparedStmt->execute();

// Indicate to the user whether the insertion was successful
if ( $preparedStmt->affected_rows > 0 )
{
    echo "<p>The new product, " . $_REQUEST["ProductName"] .
            ", has been successfully added.</p>\n";
    echo "<p><a href='6-NewProduct.php'>Click here</a> " .
            "to add another product.</p>\n";
}
else
{
    echo "<p>" . $_REQUEST["ProductName"] . " was not inserted.</p>\n";
    print "<p><a href='#' onclick='history.back(); return false;'>" .
            "Click here</a> to try to add that product again.</p>\n";
}
        
?>
        </div>
    </body>
</html>
