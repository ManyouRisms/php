<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Learning Outcome 6</title>
    </head>
    <body>
        <h1>Learning Outcome 6 - Modify a database</h1>
        <ol>
            <li><a href="6-NewProduct.php">Add a new product</a></li>
            <li><a href="6-SelectSupplier.php">Update a Supplier</a></li>

        </ol>
     </body>
</html>
