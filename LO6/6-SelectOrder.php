<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Delete Order Line Item</title>
    </head>
    <body>
        <h1>Delete Order Line Item</h1>
        
<?php

// Connect to the database
$db = new DbObject();

// Query the database
// In our select box, we want to display the order ID, and we also want to
// use the order ID as the value of the option.  To make things easier, let's
// just select the OrderID twice: the first one is used as the value of the
// option, and the second is the text to be displayed of the option.
$qryResults = $db->select( "OrderID, OrderID", "Orders", "", "OrderID" );

// Build the option list
$optionList = DbObject::createArray( $qryResults );

// We're done with the results -- free them
$qryResults->free();

// Create the order selection form
$form = new HtmlForm();
$form->renderStart( "orderForm", "Select an order to delete line items from",
        false, "6-SelectProduct.php", "POST" );
$form->renderSelect( "OrderID", "Order number", $optionList );
$form->renderSubmitReset( "subOrder", "Submit" );
$form->renderEnd();

        ?>
    </body>
</html>
