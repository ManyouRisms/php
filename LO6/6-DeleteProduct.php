<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Delete Order Line Item Confirmation</title>
    </head>
    <body>
        
        <?php

// IF an order ID has been specified
if ( isset( $_REQUEST["OrderID"] ) ) {
    // IF any product IDs have been specified
    if ( isset( $_REQUEST["ProductID"] ) ) {
        // Connect to the database
        $db = new DbObject();
        
        // LOOP for all of the specified product IDs
        foreach ( $_REQUEST["ProductID"] as $productID ) {
            // Execute a query to obtain the product name for this product ID
            $prodNameResults = $db->select( "ProductName", "Products",
                    "ProductID='" . $productID . "'" );
            
            // IF the product name query succeeded
            if ( $prodNameResults ) {
                // Obtain the product name from the query results
                $productRow = $prodNameResults->fetch_assoc();
                $productName = $productRow["ProductName"];
                
                // Free the product name result set
                $prodNameResults->free();
                
                // Delete the line items with this
                // product ID / order ID combination
                $numDeleted = $db->delete( "OrderDetails",
                        "ProductID='" . $productID . "' AND OrderID='" .
                        $_REQUEST["OrderID"] . "'" );
                
                // IF any rows were deleted
                if ( $numDeleted > 0 ) {
                    // Display a confirmation message
                    echo "<p>$productName has been successfully deleted " .
                            "from order " . $_REQUEST["OrderID"] . "</p>\n";
                } else {
                    // Display an error message
                    echo "<p>An error occurred while trying to delete " .
                            "$productName from order " . $_REQUEST["OrderID"] .
                            "</p>\n";
                }
            } else {
                // Generate message indicating product name lookup was
                // unsuccessful
                echo "<p>Unable to find product information for " .
                        "product ID " . $productID .
                        " -- product not deleted" .
                        " from order " . $_REQUEST["OrderID"] . "</p>\n";
            }
        }
        
        // Display a link to the original order selection page
        echo "<p><a href='6-SelectOrder.php'>Click here</a> to " .
                "select a different order.</p>\n";
    } else {
        // Provide the user with a link to return to
        // the product selection page
        echo "<p>No product was selected.</p>\n";
        echo "<p><a href='#' onclick='history.back(); return false;'>Click" .
                " here</a> to go back to select a product.</p>\n";
    }
} else {
    // Provide the user with a link to return to the order selection page
    echo "<p>No order was selected.</p>\n";
    echo "<p><a href='6-SelectOrder.php'>Click here</a>" .
            " to select an order.</p>\n";
}
        
        ?>
        
    </body>
</html>
