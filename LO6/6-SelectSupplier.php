<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Select a supplier to update</title>
    </head>
    <body>
        <h1>Update a Supplier</h1>
        <div>
<?php

// Connect to the database
$db = new DbObject();

// Query the database
$qryResults = $db->select( "SupplierID, CompanyName", "Suppliers",
        "", "CompanyName" );

// Build the option list
$supplierOptionList = DbObject::createArray( $qryResults );

// We're done with the results -- free them
$qryResults->free();

// Create the supplier selection form
$form = new HtmlForm();
$form->renderStart( "supplierForm", "Pick a supplier to update", false,
        "6-DisplaySupplier.php", "POST" );
$form->renderSelect( "SupplierID", "Supplier", $supplierOptionList );
$form->renderSubmitReset( "subSupplier", "Submit" );
$form->renderEnd();

?>
        </div>
    </body>
</html>
