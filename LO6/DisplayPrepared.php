<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>List of all products</title>
    </head>
    <body>
        <h1>List of all products</h1>
        
<?php

// Connect to the database
$db = new DbObject();

// Prepare the query statement.  Note: we're not using any bound parameters.
$qryStmt = "SELECT p.ProductID, p.ProductName, c.CategoryName " .
        "FROM Products p " .
        "JOIN Categories c " .
        "WHERE p.CategoryID=c.categoryID " .
        "ORDER BY p.ProductID";
$preparedStmt = $db->prepare( $qryStmt );

// Execute the query
$preparedStmt->execute();

// Grab all of the results from the MySQL server, and buffer them in the
// PHP client.
$preparedStmt->store_result();

// Specify which variables will contain which columns.
$preparedStmt->bind_result( $productID, $productName, $categoryName );


// Display the results in a table
echo "<table>\n";
echo "    <tr><th>Product ID</th><th>Product Name</th><th>Category</th></tr>\n";

while ( $preparedStmt->fetch() )
{
    // Each time fetch() is called, the values stored in productID,
    // productName, and categoryName will be updated with the next results.
    echo "    <tr><td>$productID</td><td>$productName</td>" .
            "<td>$categoryName</td>\n";
}

echo "</table>\n";

$preparedStmt->close();

?>
        
    </body>
</html>
