<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Supplier Update Results</title>
    </head>
    <body>
        <h1>Supplier Update Results</h1>
        <div>
<?php

unset( $_REQUEST["subSupplier"] );
//var_dump( $_REQUEST );


$db = new DbObject();


$affectedRows = $db->update( $_REQUEST, "Suppliers", "SupplierID" );


if ( $affectedRows > 0 )
{
    echo "<p>" . $_REQUEST["CompanyName"] .
            " was updated.</p>\n";
    echo "<p><a href='6-SelectSupplier.php'>Click</a> " .
            "to go back</p>\n";
}
else
{
    echo "<p>" . $_REQUEST["CompanyName"] . " was not updated.</p>\n";
    print "<p><a href='#' onclick='history.back(); return false;'>" .
            "Click</a> to go back and try again.</p>\n";
}

?>
        </div>
    </body>
</html>
