<?php

function my_autoloader($className)
    {
        require_once("classes/$className.class.php");
    }
    spl_autoload_register("my_autoloader");

/******* BBBBBBBONUS LEARNAGGGGEE ******
 * Only use this if you are awesome and not SHANE
 * Using PHP pointer variables
 */
$pntrPostSubmitLogin = &$_POST["submitLogin"];
$pntrPostUsername = &$_POST["username"];
$pntrPostPassword =  &$_POST["password"];


//if the user already logged in 
if(isset($pntrSessionLoggedIn) && $pntrSessionLoggedIn)
{
    // redirect the user to the check login page
    header("Location: 7-CheckLogin.php");
    exit();
}


if(isset($pntrPostSubmitLogin))
{
    $passwordChecker = new PasswordChecker();
    //check if required fields are posted and whether they match our password array
    if(!empty($pntrPostUsername))
    {
        // for now we are simply going to set the user as logged in
        // when they view the page
        $pntrSessionLoggedIn = $passwordChecker->isValid($_POST["username"],$_POST["password"]);

        //when elevating the user's rights you should regenerate the session id
        // in case the previous session id was stolen
        session_regenerate_id(true); //true will delete the old session
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo7-7-Login</title>
        <link type="text/css" rel="stylesheet" href="form-table.css" />        

    </head>
    <body>
        <h1>lo7-7-Login</h1>
<?php

if(!$pntrSessionLoggedIn)//not logged in
{    
    if(isset($pntrPostSubmitLogin))// form posted but not logged in
    {
        echo "<div>Invalid username or password</div>";
    }
    // Create the login form
    $form = new HtmlForm();
    $form->renderStart( "loginForm", "Please log in" );
    $form->renderTextbox( "username", "User Name", true, 40 );
    $form->renderPassword( "password", "Password", true, 40 );
    $form->renderSubmitReset( "submitLogin", "Login" );
    $form->renderEnd();
} else {
?>
        <div>
            <p>You have been logged in</p>
            <p> <a href="7-LogOut.php"> Log out of website </a></p>
            <p><a href="7-CheckLogin.php"> Check your log in status </a></p>
        </div>       
<?php
}
?>
    </body>
</html>
