<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add a new user to the website</title>    
        <link type="text/css" rel="stylesheet" href="form-table.css" />        

    </head>
    <body>
        <h1>LO7-7-AddLogin</h1>
        <div>
    <body>
<?php

if ( !isset( $_POST["subNew"] ) )
{
    // Create the new user form
    $form = new HtmlForm();
    $form->renderStart( "newUserForm",
            "Enter the information for the new user" );
    $form->renderTextbox( "username", "User Name", 40 );
    $form->renderPassword( "password", "Password", 40 );
    $form->renderPassword( "password2", "Re-enter password", 40 );
    $form->renderSubmitReset( "subNew", "Add new user" );
    $form->renderEnd();
}
else
{
    // Verify that the entered passwords match
    if ( $_POST["password"] != $_POST["password2"] )
    {
        // Warn the user that the passwords don't match, and let him
        // try again
        echo "<p>The entered passwords do not match</p>";
        echo "<p><a href='#' onclick='history.back(); return false;'>Click";
        echo "    here</a> to try again</p>";
    }
    else
    {
        // Create an instance of the password checker
        $passwordChecker = new PasswordChecker();

        // Add the new user
        $user = $_POST["username"];
        $pwd = $_POST["password"];
        $success = $passwordChecker->addUser( strip_tags($user), $pwd );

        if ( $success )
        {
            echo "<p>The user $user has been added</p>";
            echo "<p><a href='7-AddLogin.php'>Click here</a>\n";
            echo "    to add another</p>\n";
        }
        else
        {
            echo "<p>We failed to add $user to the website</p>";
            echo "<p><a href='#' onclick='history.back(); return false;'>Click";
            echo "    here</a> to try again</p>";
        }
    }
}

?>
    </body>
</html>
