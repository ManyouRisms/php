<?php
    session_start();

// mimics a page that requires a user to be authenticated
// will be our secured page

    // once session is started we can use a conditional statement
    // that checks session variables
    if (!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"])
    {
        // now the redirection
        header("Location: 7-Login.php"); 
        exit(); 
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo7-7-CheckLogin-Oct 28, 2014</title>
    </head>
    <body>
        <h1>lo7-7-CheckLogin</h1>
        
        
        <div>

            <?php
                

            if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"])
            {
            ?>          
            
            <p> You are logged in</p>
            <a href="7-LogOut.php">Click to log out</a>
            <?php
            
            }else{
                
            ?>
            
            <p> You are NOT logged in</p>
            <a href="7-Login.php">Click to log in</a>
            <?php 
            }
            ?>


        </div>
    </body>
</html>
