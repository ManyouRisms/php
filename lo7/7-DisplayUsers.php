<?php

function my_autoloader( $className )
{
    require_once( "classes/$className.class.php" );
}

spl_autoload_register( "my_autoloader" );

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Display all users</title>
    </head>
    <body>
        <h1>List of all users</h1>
<?php

// Connect to the database
$db = new DbObject();

// Query the database
$qryResults = $db->select( "userID, username", "Password" );

// IF the query succeeded
if ( $qryResults )
{
    // Display the records
    DbObject::displayRecords( $qryResults );
}

?>
    </body>
</html>
