<?php

/**
 * The PasswordChecker allows us to check if an entered username and
 * password is valid.
 *
 * @author ins214
 */
class PasswordChecker
{
    private $password;

    /**
     * Purpose: Create a password checker object.
     */
    public function __construct()
    {
    }
    
    /**
     * Purpose: Determine whether the supplied username and password
     *   combination is valid
     * @param string $username The username to check
     * @param string $password The password to check
     * @return boolean TRUE if the username / password combination is valid,
     *   and FALSE otherwise
     */
    public function isValid( $username, $password )
    {
        // By default, the combination is not valid
        $valid = FALSE;
        
        // Open a database connection
        $db = new DbObject();
        
        // Query for the password for the specified username
        $qryResult = $db->select( "password", "Password",
                "username='" . $db->escape( $username ) . "'" );
        
        // If there was one row returned, check the password against
        // the supplied password
        if ( $qryResult->num_rows == 1 )
        {
            $passwordRow = $qryResult->fetch_row();
            if ( $passwordRow[0] == password_verify( $password,
                                                     $passwordRow[0] ) )
            {
                $valid = TRUE;
            }
        }
        
        // Return whether the username and password combination is valid
        return $valid;
    }

    /**
     * Purpose: Add a user into the password list
     * @param string $username The username to add
     * @param string $password The password associated with the username
     * @return boolean TRUE if the user was successfully added,
     *   FALSE otherwise
     */
    public function addUser( $username, $password)
    {
        // Open a database connection
        $db = new DbObject();

        // Create the array to use with the insert method
        $record["username"] = $db->escape( $username );
        $record["password"] = password_hash( $password, PASSWORD_DEFAULT );
        // Insert the user into the Password database
        $numRows = $db->insert( $record, "Password" );

        return ( $numRows == 1 );
    }

}
