<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Learning Outcome 4 - Connecting to a database Oct 2</title>
    </head>
    <body>
        <h1>LO4 - Connecting to a database - Oct2</h1>
        <ol>
            <li><a href="4-Procedural.php">The procedural approach</a></li>
            <li><a href="4-Object.php">The object-oriented approach</a></li>
            <li><a href="4-OurObject.php">The object-oriented approach with 
                    our own object.</a></li>
        </ol>
        <?php
        // put your code here
        ?>
    </body>
</html>
