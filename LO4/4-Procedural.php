<?php
// start by connecting to the database 
//mysqli_connect($host, $user, $password, $database, $port, $socket)
// host - host of hte mysql server
// user - mysql account name
// password - password for mysql
// database name - schema name use by default
// port - mysql port you connect to
// socket - if you're using sockets

error_reporting(null);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO4-4-Procedural-Oct 2, 2014</title>
    </head>
    <body>
        <h1>LO4-4-Procedural</h1>
        <div>

            <?php
            // just doing this here so some HTML gets put out even if theres an
            // error. in production you would normally do this in the top
            $db = @mysqli_connect("kelcstu06", "CST221", "ZRBFIA", "CST221");
            if ($db)
            {
                echo "Connected";
                var_dump($db);
                mysqli_close($db);
     
                echo "<div>Closed</div>";
            }
            else
            {
                
                die("<div><h1>Connection error<h1></div><div>". 
                mysqli_connect_error() .  "</div>");
            }
            
            
            ?>

        </div>
    </body>
</html>
