<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
error_reporting(null);
include '../classes/dbObject.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO4-4-Object-Oct 2, 2014</title>
    </head>
    <body>
        <h1>LO4-4-Object</h1>
        <div>

            <?php
            // just doing this here so some HTML gets put out even if theres an
            // error. in production you would normally do this in the top
//            $db = @mysqli_connect("kelcstu06", "CST221", "ZRBFIA", "CST221");
//            if ($db)
//            {
//                echo "Connected";
//                var_dump($db);
//                mysqli_close($db);
//     
//                echo "<div>Closed</div>";
//            }
//            else
//            {
//                
//                die("<div><h1>Connection error<h1></div><div>". 
//                mysqli_connect_error() .  "</div>");
//            }
//            
            
            $db = new mysqli("kelcstu06", "CST221", $pass, "CST221");

            if (!$db->connect_error)
            {
                echo "Connected";
                var_dump($db);
                $db->close();
                echo "Closed";
            }
            
            else
            {
                
                die("<div><h1>Connection error<h1></div><div>") . $db->connect_error;
            }            
            ?>

        </div>
    </body>
</html>
