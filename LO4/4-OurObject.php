<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags

// autoloader gets called whenever a class not found error is thrown
function my_autoloader ($className)
{
    require_once("../../classes/$className.class.php");
}

spl_autoload_register("my_autoloader");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO4-4-OurObject-Oct 6, 2014</title>
    </head>
    <body>
        <h1>LO4-4-OurObject</h1>
        <div>        
            <?php
            
            $db = new dbObject();
            echo "Success";
            ?>
            
        </div>
    </body>
</html>
