<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DBObject
 *
 * @author cst221
 */
class DbObject {
    // create a mysqli object, and assign it to the internal attribute
    // if the conection failed
        // display error message and exit
    
    // pass in same four parameters to mysqli constructor
    
    /*
     * @var mysqli The database connection
     */
    private $dbConnect;
    //public $queryResult;
    
    
    
    /**
     * Purpose: To create and open a conenction to a MySQL server
     * @param string $host - name of MySQL server
     * @param string $user - name of the MySQL user
     * @param string $password - name of the users password
     * @param string $database - name of the schema to use
     */
    public function __construct($host="kelcstu06", $user="CST221", $password="ZRBFIA", $database="CST221") 
    {
        // create sqli object and assign it to the internal object
        $this->dbConnect = new mysqli($host, $user, $password, $database);

        
        // if the connection failed
        if ($this->dbConnect->connect_error)
            {
                die("<p>Connection error") . $dbConnect->connect_error . "</p>\n";
            }
    }
    
    /***
     * Purpose: Close the database connection. The destructor gets called
     * when the object goes out of scope. (function terminates, program ends).
     * Rather than having a separate close method (which might be a good idea 
     * anyways) we'll just close the connection here.
     */
    public function __destruct()
    {
        $this->dbConnect->close();
    }
    
    /**
     * 
     * @param type $queryString Sends the query string to the database object
     */
    public function runQuery($queryString)
    {
        $queryResult = $this->dbConnect->query($queryString);
        
        // if the query fails
        if (!$queryResult)
        {
            // not the prettiest way to go about, but yeah
            echo "Query $queryString couldn't execute.";
        }
        return $queryResult;
    }
    
    
    public function select($columnList, $tableList, $condition="", $sort="", $extras="")
    {
        $queryString = "select $columnList from $tableList";
        // if condition is specified, concatenate it to the string
        if ($condition!="")
        {
            $queryString .= " WHERE $condition ";
        }
        // if $sort is specified, concatenate it to the string
        if ($sort != "")
        {
            $queryString .= " ORDER BY $sort ";
        }
        // if $extras are specified, concatenate it to the string
        if ($extras !="")
        {
            $queryString .= " $extras ";
        }
        
        //echo "Using string: <p>$queryString</p>";
        return $this->runQuery($queryString);       
    }
    
public static function displayResults( $qryResults )
    {
        // Display the opening table tag
        echo "<table>\n";
        
        // Display a thead opening tag and a table row opening tag
        echo "  <thead>";
        echo "    <tr>";
        
        // LOOP for all query result columns
        foreach ( $qryResults->fetch_fields() as $fieldInfo )
        {
            // Display the column name within a table header tag
            echo "<th>{$fieldInfo->name}</th>";
        }
        
        // Display a table row closing tag and a thead closing tag
        echo "</tr>\n";
        echo "  </thead>\n";

        // Display a tbody opening tag
        echo "  <tbody>\n";

        // LOOP for all the query rows returned
        while ( $row = $qryResults->fetch_row() )
        {
            // Display a table row opening tag
            echo "    <tr>";
            
            // LOOP for all the query result columns
            for ( $i = 0; $i < $qryResults->field_count; $i++ )
            {
                // Display the value of this query result row and column
                echo "<td>{$row[$i]}</td>";
            }
            
            // Display a table row closing tag
            echo "</tr>\n";
        }

        // Display a tbody closing tag
        echo "  </tbody>\n";

        // Display the closing table tag
        echo "</table>\n";
    }

}
