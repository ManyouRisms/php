<?php
function my_autoloader ($className)
{
    require_once("../../classes/$className.class.php");
}

spl_autoload_register("my_autoloader");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO5-5-OurObject-Oct 9, 2014</title>
    </head>
    <body>
        <h1>LO5-5-OurObject</h1>
        <div>
            
            <?php
            $db = new dbObject();

            if ($db)
            {
                //note, the numrows i created as a property recovered from 
                // 
                
                echo "Connected";
                $queryString = 'SELECT firstName, lastName FROM Instructor';
                // runs our own object's run query, which sends the query to it's db object
                
                $queryResult = $db->runQuery($queryString); 

                $numRows = $queryResult->num_rows;

                echo "<br />Fetched: " .$numRows . " results.<br />";

                echo "<table border='1'><tr><th>First Name</th><th>Last Name</th></tr>";
                while ($row = $queryResult->fetch_row())
                {
                    echo "<td>$row[0]</td>";
                    echo "<td>$row[1]</td></tr>";
                }

                
                echo "</table>";
            }
            
            else
            {
                
                die("<div><h1>Connection error<h1></div><div>") . $db->connect_error;
            }       
            
            
            ?>

        </div>
    </body>
</html>
