<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO5-5-Object-Oct 8, 2014</title>
    </head>
    <body>
        <h1>LO5-5-Object</h1>
        <div>
            
            <?php
            $db = new mysqli("kelcstu06", "CST221", "ZRBFIA", "CST221");

            if ($db)
            {
                echo "Connected";
                $queryString = 'SELECT firstName, lastName FROM Instructor';
                $result = $db->query($queryString); 
                $numRows = $result->num_rows;
                echo "<br />Fetched: " .$numRows . " results.<br />";
                $result->free();
                echo "Closed";
            }
            
            else
            {
                
                die("<div><h1>Connection error<h1></div><div>") . $db->connect_error;
            }       
            
            
            ?>
            
            

        </div>
    </body>
</html>
