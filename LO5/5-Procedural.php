<?php
// start by connecting to the database 
//mysqli_connect($host, $user, $password, $database, $port, $socket)
// host - host of hte mysql server
// user - mysql account name
// password - password for mysql
// database name - schema name use by default
// port - mysql port you connect to
// socket - if you're using sockets

//error_reporting(null);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO5-Procedural-Oct 7, 2014</title>
    </head>
    <body>
        <h1>LO5-Procedural</h1>
        <div>

<?php
// just doing this here so some HTML gets put out even if theres an
// error. in production you would normally do this in the top
$db = mysqli_connect("kelcstu06", "CST221", "ZRBFIA", "CST221");
if ($db)
{
    echo "Connected";
    $queryString = 'SELECT firstName, lastName FROM Instructor';
    $result = mysqli_query($db, $queryString);
   
    if ($result)
    {
        $numRows = mysqli_num_rows($result);
        mysqli_free_result($result);
        echo "<br />Fetched: " .$numRows . " results.<br />";
    }
    
    else
    {
        echo "Failed to fetch.";
    }
    
   
}

?>
        </div>
    </body>
</html>
