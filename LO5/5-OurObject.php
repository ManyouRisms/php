<?php
function my_autoloader ($className)
{
    require_once("../../classes/$className.class.php");
    
}

spl_autoload_register("my_autoloader");

require('../../classes/DbObject.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO5-5-OurObject-Oct 9, 2014</title>
    </head>
    <body>
        <h1>LO5-5-OurObject</h1>
        <div>
            
            <?php
            $db = new DbObject();

            if ($db)
                
            {
                $queryString = 'SELECT firstName, lastName FROM Instructor';

               $queryResult = $db->runQuery($queryString); 

                
            }
            else
            {
                
                die("<div><h1>Connection error<h1></div><div>") . $db->connect_error;
            }       
//            $queryResult->data_seek(0);
//            DbObject::displayResultsNicely($queryResult);

                DbObject::displayResultsNicely($queryResult);
            ?>

        </div>
    </body>
</html>
