<?php
// "IMPORT" keyword in Java is used to include libraries to your application
// PHP has four ways to include code from another file:
// include
// include_once
// require
// require_once
// the only difference is that include displays a warning if the file is not found
// and require will throw an error, halting all other processing.

require 'ls1-createclasses.php'; // specify the filename, not the classname.

// now lets use our student class
$student1 = new Student(42, "Bubsled");
$student1->firstname = "Breydon";

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo2-ls2-instantiateclasses-Sep 15, 2014</title>
    </head>
    <body>
        <h1>lo2-ls2 - Use the class we defined in ls1</h1>
        <pre>

            <?php
                // display code block
                // example of a very useful debug function
                var_dump($student1);
                
                $student1->phone = "8675309";
                var_dump($student1);

            ?>

        </pre>
    </body>
</html>
