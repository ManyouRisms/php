<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
session_start();
session_regenerate_id(true);
//include './ls5-ssl.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo2-ls4-sessiontest-Sep 17, 2014</title>
    </head>
    <body>
        <h1>lo2-ls4-sessiontest</h1>
        <div>
            <h1>Below is a session test.</h1>
            <?php
                if (isset($_SESSION["fullname"]))
                {
                    echo "Hi again, $_SESSION[fullname]";
                    
                }
            ?>

            
        </div>
            <h1>Below is a cookie test.</h1>
            <?php
                if (isset($_COOKIE["fullname"]))
                {
                    echo "Hi again, $_COOKIE[fullname]";
                    
                }
            ?>
        <div>
            
            
        </div>
    </body>
</html>
