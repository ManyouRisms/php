<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags

session_start(); // be sure to put this line of code at the very top of the file.
// you will also need to put it on every file where you want to use sessions.

session_regenerate_id(true); // for security when escalating permissions, be sure
// to regenerate the session id. example: when user logs in as administrator.

/*
 * Sessions
 * 
 */

if (isset($_GET["logout"]))
{
    // instead of setting a session and cookie - the user indicated to clear out
    // the session and cookie
    
    /**
     *  There are 3 methods to remove session information
     */
    
    // method 1: remove a single value pair from session array
    unset($_SESSION["fullname"]);
    
    // method 2: remove all session key/value pairs for the current session
    session_unset();
    
    // method 3: remove all session key/value pairs and destroy associated data
    //  file on server
    session_destroy();
    
    // remove a cookie - by setting a cookie that expires in the past. the browser
    //  will delete it
    setcookie("fullname", "Matieschin, Steve", time() - 3600 );
    
}
else
{
    //in code we will set a session value
$_SESSION["fullname"] = "Paulyle Boechwall";

// hard coding a cookie using php syntax
// wha wha wha this is wrong, because its putting a value in the servers supergloba;
// for cookie, but it doesn't create that cookie on any machine, therefore nothing 
// will change. In order to set a cookie on the browser, we have to use a function.
$_COOKIE["fullname"] = "Breydon Bubsled";

// this sets a cookie key: val as fullname: matieschin, steve, for one hour.

//?logout=1


setcookie("fullname", "Matieschin, Steve", time() + 3600 );
}


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo2-ls4-sessions-Sep 17, 2014</title>
    </head>
    <body>
        <h1>lo2-ls4-sessions</h1>
        <div>

            <?php
            // display code blocks here. this is where you will echo HTML elements
            // on the web page. try to minimize nesting PHP tags within HTML tags
            foreach ($_SERVER as $property) 
//            {
//                echo var_dump($property);
//            }
                
                
            ?>

        </div>
                <a href="ls4-sessiontest.php">Test session variables</a>
    </body>
</html>
