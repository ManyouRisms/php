<?php
// logic block
// set up your code here to minimize the amount of PHP tags nested within HTML 
//   tags
//require '../../Classes/HtmlForm.class.php';
include '../classes/HtmlForm.class.php';

$colours = array("FF0000"=>"Red", "00FF00"=>"Green", "0000FF"=>"Blue");
$form = new HtmlForm();
$hobbies = array("Kite Flying", "Catfishing", "Working out", "Salsa Dancing", "Piledriving");
if (isset($_POST["courses"]))
{
    // since the select box is named courses[] php will parse values into an array
    foreach ($_POST["courses"] as $course)
    {        
        echo "<li>$course</li>";
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo2-htmlformtest-Sep 18, 2014</title>
    </head>
    <body>
        <h1>lo2-htmlformtest</h1>
        <div>

            <?php
            // display code blocks here. this is where you will echo HTML elements
            // on the web page. try to minimize nesting PHP tags within HTML tags
            $form->renderStart("testform","HTML form legend" );
            
            $form->renderTextbox("firstname", "First Name", true);
            $form->renderSelect("testpost", "Favourite Colours", $colours);
            $form->renderSubmitEnd("testpost", "Test your html class");
            $form->renderMultiSelect
            
            ?>
            
            <select name="courses[]" multiple >
                
                <option value="cweb280">Advanced web programming II </option>
                <option value="cnet280">Advanced Networking </option>
                <option value="cdbm280">Intro to databases</option>
                
            </select>

        </div>
    </body>
</html>
