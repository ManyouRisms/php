<?php

/*
 * Normally the filename would be like "ClassName.class.php". For this example 
 * only, we are using the learning outcome naming scheme.
 */


/**
 * Description of Student
 * a usual SIAST student object
 * 
 *
 * @author cst221
 */
class Student 
{

    public $firstname;
    public $lastname;
    
    // for a later example define protected variable
    protected $phone;
    protected $address;
    
    // create private variables for the example
    private $id; // just an intenger you send in - something like an id in a db table
    private $username; // the siast username : lastname + 4 random numbers (zero padded)
    
    /**
     * Our constructor takes in the following params
     * $id - used as a unique identifier
     * $lastname - used to generate a siast username
     * $firstname - (optional) - because when you ask for the last name you usually
     *     get the first name too
     * 
     */
    // using same variable names as attributes in the class
    function __construct($id, $lastname, $firstname="") 
    { 
        // note: you must put a single dollar sign at the beginning of the 
        // full name of the property
        $this->id = $id; //same name as property, so we use $this-> to denote
                        // that it is a property of the current class.
        
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        
        // call the function to create siast-esque username with the last name 
        // we pass in.
        $this->generateUserName($lastname);
        
    }
    
    //minicise - create function that takes in last name and creates a siast
    // username
    
    private function generateUserName($lastname)
    {
        // get random 4 digit number
        $randomNumber = rand(1,9999);
        
        //$formatString = "{%04d}.$randomNumber";
        $formatString = "%s%04d";        
        
        // use sprintf and a format string to zero pad the integer
        $this->username = sprintf($formatString, $lastname, $randomNumber);
        
        //  hint: %04d in the format string zero pads to 4 digits
    }    
}
