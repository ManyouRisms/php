<?php
// if https is not setup at all, then check what the value of server https
// is. (true or false)
if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"])
{
    // redirect user to the secure version of the request
    // tell the browser the page has moved for good.
    header("HTTP/1.1 301 Moved Permanently");
    
    // now tell the browser where the new location is
    // (the new location is simply https:// instead of http://
    
    //use for assignement to redirect to login page
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]); 
                                                  // domain name of server
                                                    //concatenated to https, then The path.
 exit(); // tell php to stop all other executio of any following code    
    
 /**
  * ssl encrypts the communication between the server and the client browser. 
  * the https protocol uses ssl to secure a website response and client's request
  * this standard is used when dealing with private and or secure personal
  * information -- often used for financial transactions on the web. 
  * 
  * SSL is NOT a function of PHP or any other web based language. it is part of
  * the webserver softwarep package (IE apache, or IIS). it is often used to meet
  * privacy laws like in Canada and US.
  */
}
