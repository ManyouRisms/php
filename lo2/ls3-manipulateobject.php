<?php

/*
 * This is an example of how to define a class in a php webpage.
 */

require './ls1-createclasses.php';

/**
 * Description of ls3-manipulateobject
 *
 * @author cst221
 */
class CSTStudent extends Student { // use classname for inheriting, not filename
//put your code here
    public $account;
    public $group;
    public $yearInCourse;
    
    // if declaring an optional parameter, all proceeding parameters must be optional.
    function __construct($id, $lastname, $firstname = "", $account="", $phone="", $address="") {
        parent::__construct($id, $lastname, $firstname);

        $this->account = $account;
        $this->address = $address; // these are protected but we can access them
        $this->phone = $phone;  
    }
    
}

$cstStudent = new CSTStudent(42, "Aprand","Briollin", "cst245");

//afterclass minicise - define an array of cst students and output to the page.

$studentArray = array();
$firstNames = array("jesse", "johnyy", "marcus", "joey");
$lastNames = array("preiner", "martinez", "oreullius", "fontaine");

for ($index = 0; $index < count($lastNames); $index++) {
    //$studentArray[i] = new Student(i, $lastNames[i]);
    $randDigits = rand(0000, 9999);
    $formatString = "555-%04d";
    $ranNumber = sprintf($formatString,$randDigits);
    $studentArray[] = new CSTStudent($index, $lastNames[$index], $firstNames[$index], "", $ranNumber, "");
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo2-ls3-manipulateobject-Sep 16, 2014</title>
    </head>
    <body>
        <h1>lo2-ls3 - Manuipulate stuff</h1>
        <pre>

            <?php
                // display code block
                // example of a very useful debug function
                //var_dump($cstStudent);
                
                foreach ($studentArray as $student) {
                    echo"<ul>";
                    //var_dump($student);
                    echo"<li><label>First Name: </label>$student->firstname</li>";
                    echo"<li><label>Last Name: </label>$student->lastname</li>";
                    //echo"<li><label>Phone: </label>$student->phone</li>"; phone is protected
                    echo"</ul>";

                    
                }
    
            ?>

        </pre>
    </body>
</html>
