<?php

function my_autoloader($className) {
    include("../classes/$className.class.php");
}

spl_autoload_register("my_autoloader");


$provinces = array();
$fileSaved = false;

// create new xml object with filepath and rootnode name, pass in the root node
// name in case the xml file doesn't exist, the constructor will create a new one
$xml = new XmlObject("../wide_open/provinces.xml", "provinces");

// call toArray method on our xml object to get an assoc array of provinces with
// CODE as the KEY, and NAME as the VALUE
$provinces = $xml->toArray("//province", "name", "code");

//ensure the required posted fields are not empty
if (!empty($_POST["cityname"]) && !empty($_POST["citydescription"])) {
    
    // remove duplicates, makes sure to only remove city in province desired.
    // note @ is for attributes.
    $cityPath = '//code[.="' . $_POST["provincecode"] . '"]'
            . '/../cities/city[@name="' . $_POST["cityname"] . '"]' ;
    

    
    $xml->removeNodes($_POST["cityname"]);
    
    
    $xPath = '//code[.="' . $_POST["provincecode"] . '"]/../cities';

    $xml->addNodes($xPath, "city", "", array("name"=>$_POST["cityname"], 
                                      "description"=>$_POST["citydescription"]));
    

    $fileSaved = $xml->saveFormattedXml();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>lo8-ls2-SaveToXml</title>
        <link href="form-table.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>lo8-ls2-SaveToXml</h1>
        <div>
<?php
$form = new HtmlForm();
$form->renderStart("savetoxml", "Save data to XML file");
$form->renderSelect("provincecode", "Provinces", $provinces);
$form->renderTextbox("cityname", "City Name", true);
$form->renderTextbox("citydescription", "City Description", true);
$form->renderSubmitEnd("submitSave", "Save to XML");

//if form submitted and file save successfully
if (isset($_POST["submitSave"]) && $fileSaved) {
    echo "<p> The <a href='../wide_open/provinces.xml'>XML File</a> was successfully saved!</p>";
}
?>

        </div>

    </body>
</html>
