<?php
    function my_autoloader($className) {
        require_once( "../classes/$className.class.php" );
    }

    spl_autoload_register("my_autoloader");

    $outputResult = "";

    if (isset($_POST["submitimport"]))
    {
        $db = new DbObject();
        
        $rootNode = simplexml_load_file("./provinces.xml");
        
        // create prepared statements that will handle the insert
        // into the database
        
        
        /*
         * PREPARED STATEMENT
         */
        $stmtInsertProv = $db->prepare("insert into Provinces 
                (Province, Code, Population, Density, Mime, Flag)
                 values (?,?,?,?,?,?)");
        
        
        $placeHolderBlob = null;
                
        $stmtInsertProv->bind_param("ssidsb", $provinceName, $provinceCode,
                $population, $density, $mime, $placeHolderBlob);
        
        
        $stmtInsertCity = $db->prepare("insert into Cities
                (ProvinceID, City, Description) values 
                (?,?,?)");
        $stmtInsertCity->bind_param("iss", $provinceID, $cityName, $description);
        
        // loop through the xml file to get the provinces
        
        foreach ($rootNode->children() as $provinceNode)
        {
            $provinceName = (string) $provinceNode['name'];
            $provinceCode = (string) $provinceNode->code;
            $population = (string) $provinceNode->population;
            $density = (string) $provinceNode->population['density'];
            $mime = (string) $provinceNode->flag['mime'];

            // get the flag image base 64 string from the xml file
            $imgData = (string) $provinceNode->flag;
            if (!empty($imgData))
            {
                // convert base64 string back into binary
                $imgData = base64_decode($imgData);
                
                // weird thing about blobs, need the placeholder variable in 
                // bind param, and will never use it again. we need to use a 
                // separate method to send the blob
                $stmtInsertProv->send_long_data(5, $imgData); // 0 based column
                
                
                
            }
            $stmtInsertProv->execute();
            
            // get the last inserted autonumber from the database
            $provinceID = $stmtInsertProv->insert_id; // once for each province
            
            
            $outputResult .= "<p>Province: $provinceName inserted id: $provinceID</p><ul>" ;
            
            // loop through the city elements in the cities node
            foreach ($provinceNode->cities->children() as $cityNode) 
            {
                // set the bound variables
                $cityName = (string)$cityNode['name'];
                $description = (string) $cityNode['description'];
                
                $stmtInsertCity->execute();
                $outputResult .= "<li>City: $cityName inserted "
                        . "id: $stmtInsertCity->insert_id</li>";
            }
            $outputResult .= "</ul>";
            
        }
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LO8-ls3-XmlToDb-Nov 20, 2014</title>
        <link href="form-table.css" rel="stylesheet" type="text/css"/>        
    </head>
    <body>
        <h1>LO8-ls3-XmlToDb</h1>
        <div>

<?php
    $form = new HtmlForm();
    $form->renderStart("xmltodb", "Save data to DB from XML");
    $form->renderSubmitEnd("submitimport", "Import to DB", "Cancel" );
    echo $outputResult
?>

        </div>
    </body>
</html>
