<?php

function my_autoloader($className) {
    require_once( "../classes/$className.class.php" );
}

spl_autoload_register("my_autoloader");

$provinces = array();
$fileSaved = false;
$fileChanged = false;

//create new XmlObject with xml file path and root node name
// we pass in the root node name in case the xml file does not exist
// the constructor will create a new SimpleXml object from scratch
$xml = new XmlObject("../wide_open/provinces.xml", "provinces");



//call toArray method to get an assoc array of provinces with code as the key
// and name as the value
$provinces = $xml->toArray("//province", "name", "code");
ksort($provinces); // sort the provinces array by key


//ensure the required posted fields are not empty
if (!empty($_POST["cityname"]) && !empty($_POST["citydescription"])) {
    //xpath to find all cities that have the same name as what the user
    // submitted
    $cityXPath = '//code[.="' . $_POST["provincecode"] . '"]/../cities' .
            '/city[@name="' . $_POST["cityname"] . '"]';
    //remove the cities with the same name to prevent duplicates
    // call the xml object's removeNodes method to remove duplicate city nodes
    $xml->removeNodes($cityXPath);

    $xPath = '//code[.="' . $_POST["provincecode"] . '"]/../cities';


    //call the xmlobject's addNodes method to add the city node to the cities node
    $xml->addNodes($xPath, "city", "", array("name" => $_POST["cityname"],
        "description" => $_POST["citydescription"]));

    // advise the application that the xml objecthas changed
    $fileChanged = true;
}
    /*     * ********* Logical Block ************* */
//ensure the required province posted fields are not empty
    if (!empty($_POST["province"]) && !empty($_POST["abbrev"]) &&
            !empty($_POST["population"]) && !empty($_POST["density"])) {
        //set defaults for uploaded file data
        $imgData = "";
        $mime = "";

        //check if a file was uploaded - remember to check is_uploaded_file on the tmp file name
        if (isset($_FILES["flag"]) && is_uploaded_file($_FILES["flag"]["tmp_name"])) {
            //read the temporary file - no need to move it to the wide open folder
            $imgData = file_get_contents($_FILES["flag"]["tmp_name"]); //get binary data
            //encode binary data to string representation
            $imgData = base64_encode($imgData);
            $mime = $_FILES["flag"]["type"]; // get the image mime type
        }

        //remove duplicate province nodes
        $provXPath = '//province[@name="' . $_POST["province"] . '"]';
        $xml->removeNodes($provXPath);

        //create new SimpleXmlElement from scratch for new province data
        $provNode = new SimpleXMLElement('<province name="' . $_POST["province"] . '" />');
        $provNode->addChild("code", $_POST["abbrev"]);

        $popNode = $provNode->addChild("population", $_POST["population"]);
        $popNode->addAttribute("density", $_POST["density"]);

        $provNode->addChild("cities");

        $flagNode = $provNode->addChild("flag", $imgData);
        $flagNode->addAttribute("mime", $mime);
        $flagNode->addAttribute("encoding", "base64");

        // import the new SimpleXmlObject into our XmlObject
        // add the new province to the provinces node (aka root node)
        $xml->importSimpleXml("/provinces", $provNode);

        $fileChanged = true;
    }

if ($fileChanged) {
    // save to xml file on disk
    $fileSaved = $xml->saveFormattedXml();
}
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>lo8-ls2-SaveToXml</title>
            <link href="form-table.css" rel="stylesheet" type="text/css"/>
        </head>
        <body>
            <h1>lo8-ls2-SaveToXml</h1>
            <div>
                
                
<?php
$form = new HtmlForm();

/*     * ************** Display Block ******************** */
if (isset($_GET["add"]) && $_GET["add"] == "city") {
    //link to switch to add province form
    echo "<p><a href='?add=province'>Add Province</a></p>";
    //add city form 


    $form->renderStart("savetoxml", "Save data to XML file");
    $form->renderSelect("provincecode", "Provinces", $provinces);
    $form->renderTextbox("cityname", "City Name", true);
    $form->renderTextbox("citydescription", "City Description", true);
    $form->renderSubmitEnd("submitSave", "Save to XML");
} else {
    //add province form
    $form->renderStart("saveprovince", "Add Province to XML File", true);
    $form->renderTextbox("province", "Province Name", true);
    $form->renderTextbox("abbrev", "Province Code", true);
    $form->renderTextbox("population", "Province Population", true);
    $form->renderTextbox("density", "Province Density", true);

    $form->renderFileInput("flag", "Flag Image");
    $form->renderSubmitEnd("submitprov", "Save to XML");

    /*         * ************** Display Block ******************** */
    if (isset($_GET["add"]) && $_GET["add"] == "city") {
        //link to switch to add province form
        echo "<p><a href='?add=province'>Add Province</a></p>";
        //add city form 

        /*             * ** put your city form in here *** */
    } else {
        //link to switch to add city form
        echo "<p><a href='?add=city'>Add City</a></p>";
    }
}



//if form submitted and file save successfully
    if ($fileSaved) {
        echo "<p> The <a href='../wide_open/provinces.xml'>XML File</a> was successfully saved!</p>";
    }
    ?>

        </div>

    </body>
</html>
