DROP TABLE IF EXISTS Cities;

DROP TABLE IF EXISTS Provinces;


CREATE TABLE Provinces (
  
ProvinceID INT NOT NULL AUTO_INCREMENT ,
  
Province VARCHAR(30) NOT NULL ,
  
Code VARCHAR(2) NULL ,
  
Population INT NULL ,
  
Density DECIMAL(5,2) NULL ,
  
Mime VARCHAR(12) NULL ,
  
Flag BLOB NULL ,
  
PRIMARY KEY (ProvinceID) 
  
)

ENGINE = InnoDB;



CREATE  TABLE Cities (
  
CityID INT NOT NULL AUTO_INCREMENT ,
  
ProvinceID INT NOT NULL ,
  
City VARCHAR(40) NOT NULL ,
  
Description VARCHAR(100) NULL ,
  
PRIMARY KEY (CityID) )

ENGINE = InnoDB;



ALTER TABLE Cities
  
ADD CONSTRAINT fk_provinces_cities
  
FOREIGN KEY (ProvinceID)
  
REFERENCES Provinces (ProvinceID)
  
ON DELETE CASCADE
  
ON UPDATE CASCADE
, 
ADD INDEX fk_provinces_cities_idx (ProvinceID ASC) ;



-- can delete provinces without deleting cities
 SET SQL_SAFE_UPDATES =0;

 DELETE FROM Cities;
 DELETE FROM Provinces;
 SET SQL_SAFE_UPDATES =1;